from ligonier_api.calls.base import BaseAPICall

class GetStudentEntitlements(BaseAPICall):
    """
    API call for retrieving a student's Mag+ entitlements.

    Last known signature in the WSDL::

        getEntitlements(
            xs:string key,
            xs:long accountNumber,
        )
    """

    @property
    def service_method(self):
        return self.api.get_service.getEntitlements

    def _pre_process(self):
        """
        Perform some validation.
        """

        self._validate_account_number()

    def _post_process(self, response):
        """
        Does some light validation.
        """

        response_list = response[0]

        # Rather than fail noisily in the face of no results coming back,
        # just return an empty list. By this point, we've already ran
        # get_student(), so we're not interested in dealing with the
        # false alarms.

        try:
            if response_list[0] == "NULL":
                # This happened before with invalid account numbers, but
                # as of November 2012, it may no longer do this. We'll leave it
                # here just in case.
                return []
            elif "No records returned for Account Number" in response_list[0]:
                # Either an invalid account number, or no entitlements. Stinks
                # we have to do a substring search, but all returned values
                # are a list of strings, instead of an object that has
                # ErrorCode/ErrorMessage attribs.
                return []
        except IndexError:
            # This was some kind of mal-formed response. Doesn't happen
            # very often, but it has.
            return []
        
        # This is kind of goofy, but the list of entitlements are under the
        # string property on the ArrayOfString.
        issue_int_set = set()
        for issue_num in response.string:
            # Have to cast to an int, because these are strings coming out
            # of the middleware. The set helps us de-dupe.
            try:
                issue_int_set.add(int(issue_num))
            except (TypeError, ValueError):
                # We get goofy values in here periodically. Skip any of these.
                continue
        return issue_int_set
