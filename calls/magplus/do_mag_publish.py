from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import APIErrorFromServiceException, APIInvalidInputException

class DoMagPublish(BaseAPICall):
    """
    API call for publishing a Mag+ issue.

    Last known signature in the WSDL::

        CreateGiftCard(
            xs:string IssueId,
            xs:dateTime IssueDate,
            xs:string issueDescription,
        )
    """
    @property
    def service_method(self):
        return self.api.set_service.MAGpublish

    def _pre_process(self):
        """
        Perform some validation.
        """

        # This doesn't do anything, so we're just going to pretend like it
        # doesn't exist.
        self.kwargs['issueDescription'] = ""

        issue_id = self.kwargs.get('issueId')
        if not issue_id:
            raise APIInvalidInputException("No IssueId specified.")

        try:
            int(issue_id)
        except (TypeError, ValueError):
            raise APIInvalidInputException("Invalid IssueId specified. Must be an int.")

        if not self.kwargs.get('issueDate'):
            raise APIInvalidInputException("No IssueDate specified.")

    def _post_process(self, response):
        """
        :raises: APIErrorFromServiceException if an error was returned.
        """

        return response
