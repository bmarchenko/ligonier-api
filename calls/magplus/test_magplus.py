import datetime
import unittest
from types import NoneType

from ligonier_api.exceptions import APIInvalidInputException
from ligonier_api.tests import test_util, interface_factory
from ligonier_api.tests.test_util import _create_random_test_account


class TestGetStudentEntitlements(unittest.TestCase):
    """
    Tests LigonierAPI.get_student_entitlements().
    """

    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, issue_id):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(issue_id, int)

    def test_basic(self):
        """
        Nothing special here, just make sure it's working with normal
        valid values.
        """

        entitlements = self.api.get_student_entitlements(
            # Greg's account for testing.
            account_number='3055786',
        )

        # We should have at least one entitlement active.
        self.assertIsInstance(entitlements, set)
        self.assertGreater(len(entitlements), 0)
        for entitlement in entitlements:
            self._check_api_output(entitlement)

    def test_invalid_account_number(self):
        """
        Pass some bad values to verify validation.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.get_student_entitlements,
            account_number=-1,
        )

        self.assertRaises(APIInvalidInputException,
            self.api.get_student_entitlements,
            account_number='a',
        )

    def test_not_student(self):
        """
        Ask for a student that doesn't exist to test the failure condition.
        """

        # We don't raise an exception here in case we run into the kind of
        # false alarms that getStudentDonationHistory raises. By the time
        # you make this call, you should have already ran get_student(), so your
        # account number should be valid. Sucks to have to make the tradeoff,
        # but it works for Ligonier.org's usage case better.
        self.assertEquals(
            self.api.get_student_entitlements(account_number=100),
            []
        )


class TestDoMagPublish(unittest.TestCase):
    """
    Tests LigonierAPI.do_mag_publish().
    """

    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, issue_id):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(issue_id, int)

    def test_basic(self):
        """
        Nothing special here, just make sure it's working with normal
        valid values.
        """

        response = self.api.do_mag_publish(
            issue_id=12345,
            issue_date=datetime.datetime.now(),
        )

    def test_invalid_issue_number(self):
        """
        Pass some bad values to verify validation.
        """

        self.assertRaises(APIInvalidInputException,
            self.api.do_mag_publish,
            issue_id='a',
            issue_date=datetime.datetime.now(),
        )
