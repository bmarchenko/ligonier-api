from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import APIErrorFromServiceException, APIInvalidInputException

class DoEventTransaction(BaseAPICall):
    """
    API call for conducting an event registration transaction.

    Last known signature in the WSDL::

        EventTransaction(
            xs:long accountNumber,
            xs:string eventCode,
            xs:string priceCode,
            xs:string token,
            xs:string authCode,
            xs:decimal authorizedAmount,
            xs:string mediaCode,
            xs:int quantity,
            xs:string attendees,
        )
    """
    @property
    def service_method(self):
        return self.api.set_service.EventTransaction

    def _parse_attendees_list(self):
        """
        Parses through the 'attendees' param, which should be a list of attendee
        dicts. Picks this apart to form the pipe-delimited list that is sent
        to the middleware as 'attendees'.

        :raises: APIInvalidInputException if anything fishy is found.
        """
        attendees_list = self.kwargs.get('attendees')

        if not isinstance(attendees_list, list):
            raise APIInvalidInputException(
                "Invalid 'attendees' param (not a list): %s" % attendees_list
            )

        soap_attendee_list = self.api.set_service_client.factory.create(
            'ArrayOfEventRegistration'
        )

        counter = 0
        for item in attendees_list:
            soap_item = self.api.set_service_client.factory.create(
                'EventRegistration'
            )

            soap_item.Title = item['title']
            soap_item.FirstName = item['first_name']
            soap_item.LastName = item['last_name']
            soap_item.Email = item['email']
            soap_item.Session = item['session']
            soap_item.SessionPrice = item.get('session_price') or 0.0
            soap_item.SessionPriceCode = "Individual"
            soap_item.SourceCode = item.get('source_code', 'WEBEVENT')
            soap_item.EventPriceCode = item['event_price_code']
            soap_item.EventPrice = float(item['event_price'])
            soap_item.AccountNumber = item.get('account_number') or 0

            soap_attendee_list.EventRegistration.append(soap_item)

            counter += 1

        # Set the attendee SOAP array as the attendees param.
        self.kwargs['attendees'] = soap_attendee_list
        # Number of attendees.
        self.kwargs['quantity'] = counter

    def _pre_process(self):
        """
        Perform some validation.
        """
        self._validate_account_number()
        self._validate_dollar_amount('authorizedAmount')

        if not self.kwargs.get('eventCode'):
            raise APIInvalidInputException(
                'No event_code specified.'
            )

        if not self.kwargs.get('priceCode'):
            raise APIInvalidInputException(
                'No price_code specified.'
            )

        if not self.kwargs.get('mediaCode'):
            self.kwargs['mediaCode'] = ''

        self._parse_attendees_list()

    def _post_process(self, response):
        """
        Checks for the presence of the ``ErrorMessage`` attrib on the response.

        :raises: APIErrorFromServiceException if an error was returned.
        """
        error_msg = getattr(response, 'ErrorMessage', None)
        if error_msg:
            # ErrorMessage key was present on the StudentDetail object returned
            # from the service. This is only present when something bad
            # happens, so raise an exception.
            raise APIErrorFromServiceException(
                '%s (Error Code: %s)' % (error_msg, response.ErrorCode),
            )

        return response