import unittest

from ligonier_api.tests import test_util, interface_factory
from ligonier_api.tests.test_util import _create_random_test_account, do_bluefin_transaction

class TestDoEventTransaction(unittest.TestCase):
    """
    Tests LigonierAPI.do_event_transaction().
    """

    def setUp(self):
        self.api = interface_factory.get_interface_obj()
        self.bluefin_api = interface_factory.get_bluefin_interface_obj()

    def _check_api_output(self, transaction):
        """
        Checks output for validity.
        """

        test_util.assertIsInstance(transaction.AccountNumber, long)
        test_util.assertIsInstance(transaction.DocumentNumber, long)
        test_util.assertIsInstance(transaction.BatchNumber, long)
        test_util.assertIsInstance(transaction.AddressId, long)
        test_util.assertIsInstance(transaction.TransactionReturn, basestring)

    def test_basic(self):
        """
        Test the default, 'send everything' behavior.
        """

        account = _create_random_test_account(self.api)
        bluefin_trans_id, bluefin_auth_code = do_bluefin_transaction()

        attendees = [
            {
                'title': 'Mr.',
                'first_name': 'Kelly',
                'last_name': 'Carper',
                'email': 'kcarper@yahoo.com',
                # This attendee is paying for a meal.
                'session': 'MEAL',
                # Price of the meal.
                'session_price': 39.00,
                'source_code': 'WEBEVENT',
                'event_price_code': 'REG-TEA',
                'event_price': 69.00,
                'account_number': 0,
            },
            {
                'title': 'Mr.',
                'first_name': 'Phil',
                'last_name': 'Liggett',
                'email': 'pliggett@yahoo.com',
                # This attendee doesn't want a meal.
                'session': '',
                'session_price': 0,
                'source_code': 'WEBEVENT',
                'event_price_code': 'REG-STU',
                'event_price': 69.00,
                'account_number': 0,
            },
        ]

        result = self.api.do_event_transaction(
            account_number=account.AccountNumber,
            bluefin_trans_id=bluefin_trans_id,
            bluefin_auth_code=bluefin_auth_code,
            total_amount=138.00,
            # At the door
            price_code='ATD',
            event_code='NAT13',
            attendees=attendees,
        )
        self._check_api_output(result)