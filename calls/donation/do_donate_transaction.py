from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import APIErrorFromServiceException, APIInvalidInputException

class DoDonateTransaction(BaseAPICall):
    """
    API call for conducting an donate transaction.

    Last known signature in the WSDL::

        DonateTransaction(
            xs:long accountNumber,
            xs:string comment,
            xs:string sourceCode,
            xs:string projectCode,
            xs:string mediaCode,
            xs:string token,
            xs:string authCode,
            xs:decimal totalAmount,
        )
    """
    @property
    def service_method(self):
        return self.api.set_service.DonateTransaction

    def _pre_process(self):
        """
        Perform some validation.
        """
        self._validate_account_number()
        self._validate_dollar_amount('totalAmount')

        if not self.kwargs.get('mediaCode'):
            self.kwargs['mediaCode'] = ''

    def _post_process(self, response):
        """
        Checks for the presence of the ``ErrorMessage`` attrib on the response.

        :raises: APIErrorFromServiceException if an error was returned.
        """
        error_msg = getattr(response, 'ErrorMessage', None)
        if error_msg:
            # ErrorMessage key was present on the StudentDetail object returned
            # from the service. This is only present when something bad
            # happens, so raise an exception.
            raise APIErrorFromServiceException(
                '%s (Error Code: %s)' % (error_msg, response.ErrorCode),
            )

        return response