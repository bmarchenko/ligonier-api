from ligonier_api.calls.base import BaseAPICall
from ligonier_api.calls.donation.exceptions import MemberPlegdgeAlreadyExists
from ligonier_api.exceptions import APIErrorFromServiceException, APIInvalidInputException

class DoMinistryPartnerTransaction(BaseAPICall):
    """
    API call for creating a recurring donor.

    Last known signature in the WSDL::

        MinistryPartnerTransaction(
            xs:long accountNumber,
            xs:string sourceCode,
            xs:string mediaId,
            xs:string projectCode,
            xs:string title,
            xs:string firstName,
            xs:string lastName,
            xs:long useOnDay,
            xs:string comment,
            xs:string creditCard,
            xs:string expMonth,
            xs:string expYear,
            xs:string cCID,
            xs:string nameOnCard,
            xs:decimal amount,
        )
    """
    @property
    def service_method(self):
        return self.api.set_service.MinistryPartnerTransaction

    def _pre_process(self):
        """
        Perform some validation.
        """
        self._validate_account_number()
        self._validate_dollar_amount('amount')

        if not self.kwargs.get('sourceCode'):
            self.kwargs['sourceCode'] = 'WEBPLEDGE'

        if not self.kwargs.get('projectCode'):
            self.kwargs['projectCode'] = ''

        if not self.kwargs.get('comment'):
            self.kwargs['comment'] = ''

    def _post_process(self, response):
        """
        Checks for the presence of the ``ErrorMessage`` attrib on the response.

        :raises: APIErrorFromServiceException if an error was returned.
        """
        error_code = getattr(response, 'ErrorCode', None)
        error_msg = getattr(response, 'ErrorMessage', None)
        if error_code == '-1003':
            raise MemberPlegdgeAlreadyExists(
                error_msg,
            )
        elif error_msg:
            # ErrorMessage key was present on the StudentDetail object returned
            # from the service. This is only present when something bad
            # happens, so raise an exception.
            raise APIErrorFromServiceException(
                '%s (Error Code: %s)' % (error_msg, response.ErrorCode),
            )

        return response