"""
Exceptions that are specific to donations.
"""
from ligonier_api.exceptions import APIException

class MemberPlegdgeAlreadyExists(APIException):
    """
    Raised when a user attempts to create a new recurring donation, but already
    has one.
    """
    pass
