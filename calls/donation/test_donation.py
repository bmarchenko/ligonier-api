import unittest
from types import NoneType
from ligonier_api.calls.donation.exceptions import MemberPlegdgeAlreadyExists

from ligonier_api.tests import test_util, interface_factory, test_defines
from ligonier_api.exceptions import APIInvalidInputException, APIErrorFromServiceException
from ligonier_api.tests.test_util import _create_random_test_account

class TestDoDonateTransaction(unittest.TestCase):
    """
    Tests LigonierAPI.do_donate_transaction().
    """

    def setUp(self):
        self.api = interface_factory.get_interface_obj()
        self.bluefin_api = interface_factory.get_bluefin_interface_obj()

    def _check_api_output(self, transaction):
        """
        Checks output for validity.
        """

        test_util.assertIsInstance(transaction.AccountNumber, long)
        test_util.assertIsInstance(transaction.DocumentNumber, long)
        test_util.assertIsInstance(transaction.BatchNumber, long)
        test_util.assertIsInstance(transaction.AddressId, long)

    def test_basic(self):
        """
        Test the default, 'send everything' behavior.
        """

        account = _create_random_test_account(self.api)

        bluefin_response = self.bluefin_api.send_request({
            'pay_type': 'C',
            'tran_type': 'S',
            'amount': 10.0,
            'card_number': test_defines.TEST_CC_NUM,
            'card_expire': test_defines.TEST_CC_EXPIR,
            'disable_cvv2': 'true',
            'bill_name1': 'First',
            'bill_name2': 'Last',
            'bill_street': '123 Some Street',
            'bill_zip': '12345',
            # Two-letter country code.
            # http://www.iso.org/iso/en/prods-services/iso3166ma/index.html
            'bill_country': 'US',
        })
        bluefin_trans_id = bluefin_response['trans_id']
        bluefin_auth_code = bluefin_response['auth_code']

        result = self.api.do_donate_transaction(
            account_number=account.AccountNumber,
            source_code='WEBDCP',
            project_code='3050.000',
            bluefin_trans_id=bluefin_trans_id,
            bluefin_auth_code=bluefin_auth_code,
            total_amount=10.0,
        )
        self._check_api_output(result)

    def test_omit_account_num(self):
        """
        Intentionally omit account number. Should raise an exception.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.do_donate_transaction,
                # Omitted.
                account_number='',
                bluefin_trans_id=1,
                bluefin_auth_code='',
                total_amount=10.0,
                project_code='DEFAULT',
                source_code='WEBDONATE',
        )

    def test_negative_amount(self):
        """
        Test sending a negative amount.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.do_donate_transaction,
                account_number=12345678,
                bluefin_trans_id=1,
                bluefin_auth_code='',
                total_amount=-10.0,
                project_code='DEFAULT',
                source_code='WEBDONATE',
        )

    def test_omitted_amount(self):
        """
        Test omitting an amount.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.do_donate_transaction,
            account_number=12345678,
            bluefin_trans_id=1,
            bluefin_auth_code='',
            total_amount=None,
            project_code='DEFAULT',
            source_code='WEBDONATE',
        )


class TestDoCreatePledgeTransaction(unittest.TestCase):
    """
    Tests LigonierAPI.do_create_pledge_transaction().
    """

    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, transaction):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(transaction.AccountNumber, long)
        test_util.assertIsInstance(transaction.DocumentNumber, long)
        test_util.assertIsInstance(transaction.BatchNumber, long)
        test_util.assertIsInstance(transaction.AddressId, long)
        test_util.assertIsInstance(transaction.TransactionReturn, (basestring, None))

    def test_basic(self):
        """
        Test the default, 'send everything' behavior.
        """
        account = _create_random_test_account(self.api)
        result = self.api.do_create_pledge_transaction(
            account_number=account.AccountNumber,
            student_title=account.Title,
            student_first_name=account.FirstName,
            student_last_name=account.LastName,
            amount=10.0,
        )
        self._check_api_output(result)

    def test_omit_account_num(self):
        """
        Intentionally omit account number. Should raise an exception.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.do_create_pledge_transaction,
                # Omitted.
                account_number='',
                student_title='Mr.',
                student_first_name='Happy',
                student_last_name='Guy',
                amount=10.0,
        )

    def test_negative_amount(self):
        """
        Test sending a negative amount.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.do_create_pledge_transaction,
                account_number=1234567,
                student_title='Mr.',
                student_first_name='Happy',
                student_last_name='Guy',
                amount=-10.0,
        )

    def test_omitted_amount(self):
        """
        Test omitting an amount.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.do_create_pledge_transaction,
                account_number=12345678,
                student_title='Mr.',
                student_first_name='Happy',
                student_last_name='Guy',
                amount=None,
        )


class TestDoCreateRecurringDonationTransaction(unittest.TestCase):
    """
    Tests LigonierAPI.do_create_recurring_donation_transaction().
    """

    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, transaction):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(transaction.AccountNumber, long)
        test_util.assertIsInstance(transaction.DocumentNumber, long)
        test_util.assertIsInstance(transaction.BatchNumber, long)
        test_util.assertIsInstance(transaction.AddressId, long)
        test_util.assertIsInstance(transaction.TransactionReturn, (basestring, None))

    def test_basic(self):
        """
        Test the default, 'send everything' behavior.
        """
        account = _create_random_test_account(self.api)
        params = dict(
            account_number=account.AccountNumber,
            student_title=account.Title,
            student_first_name=account.FirstName,
            student_last_name=account.LastName,
            cc_number=test_defines.TEST_CC_NUM,
            cc_exp_month=test_defines.TEST_CC_EXPIR_MONTH,
            cc_exp_year=test_defines.TEST_CC_EXPIR_YEAR,
            cc_cvn=test_defines.TEST_CC_CVN,
            cc_full_name=test_defines.TEST_CC_NAME_ON_CARD,
            amount=10.0,
        )
        result = self.api.do_create_recurring_donation_transaction(**params)
        self._check_api_output(result)

        # Pledge already exists for this member, we should see an exception
        # raised here.
        self.assertRaises(MemberPlegdgeAlreadyExists,
            self.api.do_create_recurring_donation_transaction,
                **params)

    def test_omit_account_num(self):
        """
        Intentionally omit account number. Should raise an exception.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.do_create_recurring_donation_transaction,
                # Omitted.
                account_number='',
                student_title='Mr.',
                student_first_name='Happy',
                student_last_name='Guy',
                cc_number=test_defines.TEST_CC_NUM,
                cc_exp_month=test_defines.TEST_CC_EXPIR_MONTH,
                cc_exp_year=test_defines.TEST_CC_EXPIR_YEAR,
                cc_cvn=test_defines.TEST_CC_CVN,
                cc_full_name=test_defines.TEST_CC_NAME_ON_CARD,
                amount=10.0,
        )

    def test_negative_amount(self):
        """
        Test sending a negative amount.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.do_create_recurring_donation_transaction,
                account_number=12345678,
                student_title='Mr.',
                student_first_name='Happy',
                student_last_name='Guy',
                cc_number=test_defines.TEST_CC_NUM,
                cc_exp_month=test_defines.TEST_CC_EXPIR_MONTH,
                cc_exp_year=test_defines.TEST_CC_EXPIR_YEAR,
                cc_cvn=test_defines.TEST_CC_CVN,
                cc_full_name=test_defines.TEST_CC_NAME_ON_CARD,
                amount=-10.0,
        )

    def test_omitted_amount(self):
        """
        Test omitting an amount.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.do_create_recurring_donation_transaction,
                account_number=1234567,
                student_title='Mr.',
                student_first_name='Happy',
                student_last_name='Guy',
                cc_number=test_defines.TEST_CC_NUM,
                cc_exp_month=test_defines.TEST_CC_EXPIR_MONTH,
                cc_exp_year=test_defines.TEST_CC_EXPIR_YEAR,
                cc_cvn=test_defines.TEST_CC_CVN,
                cc_full_name=test_defines.TEST_CC_NAME_ON_CARD,
                amount=None,
            )