from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import StudentNotFoundException

class GetStudentEmailList(BaseAPICall):
    """
    API call for retrieving student email addresses, in the case where the
    account number is known.

    Last known signature in the WSDL::

        getStudentEmail(
            xs:string key,
            xs:string accountNumber,
        )
    """

    @property
    def service_method(self):
        return self.api.get_service.getStudentEmail

    def _pre_process(self):
        """
        Perform some validation.
        """
        self._validate_account_number()

    def _post_process(self, response):
        """
        Checks the response to see if there is or isn't an account number
        that is equal to ``None``. No account number means no match.

        :raises: StudentNotFoundException when no account number match
            could be found.
        """

        emails = []

        for email in response.StudentEmail:
            if getattr(email, 'ErrorCode', None) == '-1000':
                raise StudentNotFoundException(
                    'No student found with account number: %s' % self.kwargs.get(
                        'accountNumber')
                )

            emails.append(
                {
                    'record_id': email.RecordId,
                    'account_number': email.AccountNumber,
                    'email_type': email.EmailType,
                    'email_address': email.EmailAddress,
                    'use_as_primary': email.UseAsPrimary == 1,
                    'active_email': email.ActiveEmail == 1,
                }
            )

        return emails
