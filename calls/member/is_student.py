from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import APIInvalidInputException, APIErrorFromServiceException

class IsStudent(BaseAPICall):
    """
    API call for determining whether there is a student record for the given
    first/last name, and/or

    Last known signature in the WSDL::

        isStudent(
            xs:string key,
            xs:string firstName,
            xs:string lastName,
            xs:string address1,
            xs:string email,
        )
    """

    @property
    def service_method(self):
        return self.api.get_service.isStudent

    def _pre_process(self):
        """
        Perform some validation.
        """
        if not self.kwargs.get('firstName'):
            raise APIInvalidInputException(
                'No first name specified'
            )

        if not self.kwargs.get('lastName'):
            raise APIInvalidInputException(
                'No last name specified'
            )

        email = self.kwargs.get('email')
        address1 = self.kwargs.get('address1')

        if not address1 and not email:
            # Either email or address1 must be provided.
            raise APIInvalidInputException(
                'Either email or address1 must be provided.'
            )

    def _post_process(self, response):
        """
        Checks the response to see if there is or isn't an account number
        that is equal to 0. Account number of 0 means there was no match,
        and the API call should return False instead of the SOAP response. 
        """
        if response.AccountNumber <= 0:
            # No match.
            return False

        response.PartnerStatus = response.PartnerStatus == "MP"

        if not hasattr(response, 'Title'):
            response.Title = 'Mr.'

        if not hasattr(response, 'FirstName'):
            response.FirstName = None

        if not hasattr(response, 'LastName'):
            response.LastName = None
        
        return response
