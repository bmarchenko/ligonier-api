from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import APIInvalidInputException, StudentNotFoundException

class GetStudentAddressList(BaseAPICall):
    """
    API call for retrieving a student's address list.

    Last known signature in the WSDL::

        getStudentAddress(
            xs:string key,
            xs:long accountNumber,
        )
    """

    @property
    def service_method(self):
        return self.api.get_service.getStudentAddressList

    def _pre_process(self):
        """
        Perform some validation.
        """

        self._validate_account_number()

    def _post_process(self, response):
        """
        Does some post-processing.

        :raises: StudentNotFoundException when no account number match
            could be found.
        """

        addresses = []

        for address in response.StudentAddress:
            #if getattr(address, 'ErrorCode', None) == '-1002':
            if getattr(address, 'AddressId', 0) in ["0", 0]:
                # No match.
                raise StudentNotFoundException(
                    'No student found with account number: %s' % self.kwargs.get(
                        'accountNumber')
                )

            addresses.append({
                'address_line_1': address.AddressLine1,
                'address_line_2': getattr(address , 'AddressLine1', None),
                'city': address.City,
                'state': getattr(address, 'State', None),
                'zip': getattr(address, 'Zip', None),
                'address_type': address.AddressType,
                'plus4_code': address.Plus4Code,
                'country': 'US' if address.Country == 'USA' else address.Country,
                'address_id': address.AddressId,
                'record_id': address.RecordId,
                'PhoneNumber': address.PhoneNumber,
            })

        return addresses
