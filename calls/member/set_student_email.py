from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import APIErrorFromServiceException

class SetStudentEmail(BaseAPICall):
    """
    API call for setting student phone numbers, in the case where the
    account number is known.

    Last known signature in the WSDL::

        setStudentPhone(
            string key,
            long accountNumber,
            string internationalCode,
            string areaCode,
            string phoneNumber,
            string extension
        )
    """

    @property
    def service_method(self):
        return self.api.set_service.studentEmail

    def _pre_process(self):
        """
        Perform some validation.
        """
        self._validate_account_number()

    def _post_process(self, response):
        """
        Checks for the presence of the ``ErrorMessage`` attrib on StudentDetail.

        :raises: APIErrorFromServiceException if an error was returned.
        """
        error_code = getattr(response, 'ErrorCode', None)
        error_msg = getattr(response, 'ErrorMessage', None)
        if error_msg:
            raise APIErrorFromServiceException(error_msg)
