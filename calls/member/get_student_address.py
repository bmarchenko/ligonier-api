from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import APIInvalidInputException, StudentNotFoundException

class GetStudentAddress(BaseAPICall):
    """
    API call for retrieving a student's billing or shipping address.

    Last known signature in the WSDL::

        getStudentAddress(
            xs:string key,
            xs:long accountNumber,
            xs:bool primary,
            xs:string addressType,
        )
    """

    @property
    def service_method(self):
        return self.api.get_service.getStudentAddress

    def _pre_process(self):
        """
        Perform some validation.
        """
        self._validate_account_number()

        if not self.kwargs.get('addressLine1'):
            self.kwargs['addressLine1'] = ''

    def _post_process(self, response):
        """
        Checks the response to see if the ``RecordId`` attrib is 0. In this
        case, there was no account number match.

        :raises: StudentNotFoundException when no account number match
            could be found.
        """

        if response.ErrorCode == '-1003':
            # No match.
            raise StudentNotFoundException(
                'No student found with account number: %s' % self.kwargs.get(
                    'accountNumber')
            )

        # Middleware bugs can cause these to not show up.
        if not hasattr(response, 'Zip'):
            response.Zip = None
        if not hasattr(response, 'State'):
            response.State = None
        if not hasattr(response, 'AddressLine2'):
            response.AddressLine2 = None

        # Donor Studio uses USA instead of US.
        if response.Country == 'USA':
            response.Country = 'US'

        return response
