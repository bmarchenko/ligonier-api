from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import APIErrorFromServiceException

class SetActiveEmail(BaseAPICall):
    """
    API call for activating or deactivating student phone number, in the case where the
    phone number is known.

    Last known signature in the WSDL::

        studentActivePhone(
            string key,
            long PhoneRecordId,
            int SetActivePhone
        )
    """

    @property
    def service_method(self):
        return self.api.set_service.studentActiveEmail

    def _post_process(self, response):
        """
        Checks for the presence of the ``ErrorMessage`` attrib on StudentDetail.

        :raises: APIErrorFromServiceException if an error was returned.
        """
        error_code = getattr(response, 'ErrorCode', None)
        error_msg = getattr(response, 'ErrorMessage', None)
        if error_msg:
            raise APIErrorFromServiceException(error_msg)
