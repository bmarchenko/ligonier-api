import datetime
from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import APIErrorFromServiceException, StudentNotFoundException

class GetStudentDonationSummary(BaseAPICall):
    """
    Retrieves a student's donation history.

    Last known signature in the WSDL::

        getDonationSummary(
            xs:long accountNumber,
        )
    """

    @property
    def service_method(self):
        return self.api.get_service.getDonationSummary

    def _pre_process(self):
        """
        Perform some validation.
        """
        self._validate_account_number()

    def _post_process(self, response):
        """
        Checks for the presence of the ``ErrorMessage`` attrib on StudentDetail.

        :raises: APIErrorFromServiceException if an error was returned.
        """
        error_code = getattr(response.DonationSummary[0], 'ErrorCode', False)
        if error_code:
            if error_code == '-1000':
                # Either the student had no records, or an invalid student
                # number was passed. The middleware does nothing to figure
                # out which case this is, so we'll just fail silently.
                return []
            else:
                APIErrorFromServiceException(
                    response.DonationSummary[0].ErrorMessage
                )

        donations = []

        for donation in response.DonationSummary:
            gift_date = donation.GiftDate

            parsed_date = datetime.datetime.strptime(gift_date, '%m/%d/%Y')
            new_date = datetime.date(
                parsed_date.year,
                parsed_date.month,
                parsed_date.day,
            )
            donation.GiftDate = new_date

            donations.append(donation)

        return donations