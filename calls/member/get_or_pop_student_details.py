from ligonier_api.calls.address.exceptions import InvalidShippingAddressError, InvalidBillingAddressError
from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import APIErrorFromServiceException

class GetOrPopulateStudentDetails(BaseAPICall):
    """
    API call for getting, updating, or creating a student based on the provided
    details.

    Last known signature in the WSDL::

        InquiryStudentDetailRequest(
            xs:string pageType,
            xs:string title,
            xs:string firstName,
            xs:string lastName,
            xs:string primary,
            xs:string email,
            xs:string accountNumber,
            xs:string sourceCode,
            xs:string shipCountry,
            xs:string shipAddressLine1,
            xs:string shipAddressLine2,
            xs:string shipCity,
            xs:string shipState,
            xs:string shipZip,
            xs:string billCountry,
            xs:string billAddressLine1,
            xs:string billAddressLine2,
            xs:string billCity,
            xs:string billState,
            xs:string billZip,
        )
    """

    @property
    def service_method(self):
        return self.api.inquiry_service.InquiryStudentDetailRequest

    def _pre_process(self):
        """
        Perform some validation.
        """
        if not self.kwargs.get('accountNumber'):
            self.kwargs['accountNumber'] = 0

        self._validate_account_number(is_required=False)

        blankable_fields = [
            'phoneNumber',
            'sourceCode',

            'billAddressLine1',
            'billAddressLine2',
            'billCity',
            'billZip',
            'billState',
            'billCountry',

            'shipAddressLine1',
            'shipAddressLine2',
            'shipCity',
            'shipZip',
            'shipState',
            'shipCountry',
        ]

        for field_name in blankable_fields:
            if not self.kwargs.get(field_name):
                self.kwargs[field_name] = ''

        title = self.kwargs.get('title')
        if not title or title == 'ZZ':
            # 'ZZ' was the old designator for 'Mr.' We can eventually remove
            # this, as we are sure they aren't appearing anymore.
            self.kwargs['title'] = 'Mr.'

        # Donor Studio uses USA instead of US.
        if self.kwargs.get('shipCountry') == 'US':
            self.kwargs['shipCountry'] = 'USA'

        if self.kwargs.get('billCountry') == 'US':
            self.kwargs['billCountry'] = 'USA'

    def _post_process(self, response):
        """
        Checks for the presence of the ``ErrorMessage`` attrib on StudentDetail.

        :raises: APIErrorFromServiceException if an error was returned.
        """
        error_code = getattr(response, 'ErrorCode', None)
        error_msg = getattr(response, 'ErrorMessage', None)

        if error_code in ['-1011', '-1012', '-1013', '-1014', '-1015', '-1016']:
            # These are all address-related error codes.
            raise InvalidShippingAddressError(error_msg)
        elif error_code in ['-1017', '-1018', '-1019', '-1020', '-1021', '-1022']:
            raise InvalidBillingAddressError(error_msg)
        elif error_msg:
            # ErrorMessage key was present on the StudentDetail object returned
            # from the service. This is only present when something bad
            # happens, so raise an exception.
            raise APIErrorFromServiceException(error_msg)

        if hasattr(response, 'PartnerStatus'):
            response.PartnerStatus = response.PartnerStatus == 'MP'
        else:
            response.PartnerStatus = False

        if not hasattr(response, 'Title'):
            response.Title = 'Mr.'

        if not hasattr(response, 'FirstName'):
            response.FirstName = None

        if not hasattr(response, 'LastName'):
            response.LastName = None

        if hasattr(response, 'BillStudentAddress'):
            if response.BillStudentAddress.Country == 'USA':
                response.BillStudentAddress.Country = 'US'

        if hasattr(response, 'ShipStudentAddress'):
            if response.ShipStudentAddress.Country == 'USA':
                response.ShipStudentAddress.Country = 'US'

        return response