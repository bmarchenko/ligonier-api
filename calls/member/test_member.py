import datetime
import unittest
from types import NoneType

from ligonier_api.exceptions import APIInvalidInputException, StudentNotFoundException
from ligonier_api.tests import test_defines
from ligonier_api.tests import test_util, interface_factory
from ligonier_api.tests.test_util import _create_random_test_account

class TestGetOrPopulateStudentDetails(unittest.TestCase):
    """
    Tests LigonierAPI.get_or_populate_student_details().
    """
    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, student):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(student.PartnerStatus, (bool, NoneType))
        test_util.assertIsInstance(student.AccountNumber, long)
        self.assertNotEqual(0, student.AccountNumber,
            'Middleware should never return a 0 account number.')
        test_util.assertIsInstance(student.FirstName, (NoneType, basestring))
        test_util.assertIsInstance(student.LastName, (NoneType, basestring))
        test_util.assertIsInstance(student.Email, basestring)
        test_util.assertIsInstance(student.ProjectCode, basestring)
        test_util.assertIsInstance(student.SourceCode, basestring)

    def test_basic(self):
        """
        Nothing special here, just make sure it's working with normal
        valid values. By the time this unit test is ran, the account already
        exists.
        """
        student = _create_random_test_account(self.api)
        self._check_api_output(student)
        self.assertEqual(student.BillStudentAddress.AddressLine1, '1273 Dillon Rd')
        self.assertEqual(student.Title, 'Mr.')

    def test_change_first_last_name(self):
        """
        Change first and last names, make sure this updates.
        """
        new_student = _create_random_test_account(self.api)
        # Change the name, make sure it updates.
        changed_student = self.api.get_or_populate_student_details(
            page_type='DONATE',
            title=new_student.Title,
            first_name='Blarty',
            last_name='Blartfast',
            email=new_student.Email,
            ship_country=None,
            ship_address_1=None,
            ship_city=None,
            ship_state=None,
            ship_zip=None,
            bill_country=new_student.BillStudentAddress.Country,
            bill_address_1=new_student.BillStudentAddress.AddressLine1,
            bill_city=new_student.BillStudentAddress.City,
            bill_state=new_student.BillStudentAddress.State,
            bill_zip=new_student.BillStudentAddress.Zip,
            ship_address_2=None,
            bill_address_2=new_student.BillStudentAddress.AddressLine2,
            account_number=new_student.AccountNumber,
        )
        self._check_api_output(changed_student)
        self.assertEqual(changed_student.FirstName, 'Blarty')
        self.assertEqual(changed_student.LastName, 'Blartfast')

    def test_change_primary_address(self):
        """
        Explicitly update the user's primary address.
        """
        new_student = _create_random_test_account(self.api)
        changed_student = self.api.get_or_populate_student_details(
            page_type='DONATE',
            title=new_student.Title,
            first_name=new_student.FirstName,
            last_name=new_student.LastName,
            email=new_student.Email,
            ship_country=None,
            ship_address_1=None,
            ship_city=None,
            ship_state=None,
            ship_zip=None,
            bill_country=new_student.BillStudentAddress.Country,
            bill_address_1='1274 Dillon Rd',
            bill_city=new_student.BillStudentAddress.City,
            bill_state=new_student.BillStudentAddress.State,
            bill_zip=new_student.BillStudentAddress.Zip,
            ship_address_2=None,
            bill_address_2=new_student.BillStudentAddress.AddressLine2,
            account_number=new_student.AccountNumber,
        )
        self._check_api_output(changed_student)
        self.assertEqual(changed_student.BillStudentAddress.AddressLine1, '1274 Dillon Rd')


class TestGetStudentOrderSummary(unittest.TestCase):
    """
    Tests LigonierAPI.get_student_order_summary().
    """

    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, history):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(history.ProductCode, basestring)
        test_util.assertIsInstance(history.Description, basestring)
        test_util.assertIsInstance(history.OrderDate,
            (datetime.date, NoneType))
        test_util.assertIsInstance(history.Price, float)
        test_util.assertIsInstance(history.Quantity, int)
        test_util.assertIsInstance(history.ExtPrice, float)
        test_util.assertIsInstance(history.ItemStatus, basestring)
        test_util.assertIsInstance(history.ShipDate,
            (datetime.date, NoneType))
        test_util.assertIsInstance(history.ShippingMethod,
            (basestring, NoneType))
        test_util.assertIsInstance(history.TrackingNumber,
            (basestring, NoneType))
        test_util.assertIsInstance(history.GPsop,
            (basestring, NoneType))
        test_util.assertIsInstance(history.OrderId, long)
        test_util.assertIsInstance(history.DocumentNumber, long)
        test_util.assertIsInstance(history.Amount, float)
        test_util.assertIsInstance(history.ShippingAmount, float)
        test_util.assertIsInstance(history.TaxAmount, float)

    def test_basic(self):
        """
        Nothing special here, just make sure it's working with normal
        valid values.
        """
        history = self.api.get_student_order_summary(
            # Greg's account for testing.
            account_number='3004842',
            days_back=180,
        )
        for order in history:
            self._check_api_output(order)

    def test_invalid_account_number(self):
        """
        Pass some bad values to verify validation.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.get_student_order_summary,
            account_number=-1,
        )

        self.assertRaises(APIInvalidInputException,
            self.api.get_student_order_summary,
            account_number='a',
        )

    def test_not_student(self):
        """
        Ask for a student that doesn't exist to test the failure condition.
        """
        self.assertEquals(
            self.api.get_student_order_summary(account_number=100),
            []
        )


class TestGetStudentDonationSummary(unittest.TestCase):
    """
    Tests LigonierAPI.get_student_donation_summary().
    """

    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, history):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(history.Description, basestring)
        test_util.assertIsInstance(history.GiftDate, datetime.date)
        test_util.assertIsInstance(history.GiftAmount, float)

    def test_basic(self):
        """
        Nothing special here, just make sure it's working with normal
        valid values.
        """
        history = self.api.get_student_donation_summary(
            # Greg's account for testing.
            account_number='3004842',
        )

        for donation in history:
            self._check_api_output(donation)

    def test_invalid_account_number(self):
        """
        Pass some bad values to verify validation.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.get_student_donation_summary,
                account_number=-1,
        )

        self.assertRaises(APIInvalidInputException,
            self.api.get_student_donation_summary,
                account_number='a',
        )

    def test_not_student(self):
        """
        Ask for a student that doesn't exist to test the failure condition.
        """
        self.assertEquals(
            self.api.get_student_donation_summary(account_number=100),
            []
        )


class TestIsStudent(unittest.TestCase):
    """
    Tests LigonierAPI.is_student().
    """

    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, student):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(student.AccountNumber, long)
        test_util.assertIsInstance(student.RecordId, long)
        test_util.assertIsInstance(student.DocumentNumber, long)
        test_util.assertIsInstance(student.AddressId, long)
        test_util.assertIsInstance(student.BatchNumber, long)
        test_util.assertIsInstance(student.SourceCode,
            (NoneType, basestring))
        test_util.assertIsInstance(student.FirstName, basestring)
        test_util.assertIsInstance(student.PartnerStatus, bool)
        test_util.assertIsInstance(student.LastName, basestring)
        test_util.assertIsInstance(student.Email, basestring)
        test_util.assertIsInstance(student.PhoneNumber,
                                   (NoneType, basestring))
        test_util.assertIsInstance(student.Title, basestring)
        test_util.assertIsInstance(student.AccountType, basestring)
        test_util.assertIsInstance(student.Active, bool)
        test_util.assertIsInstance(student.ErrorMessage,
            (NoneType, basestring))

    def test_basic(self):
        """
        Nothing special here, just make sure it's working with normal
        valid values.
        """
        student = self.api.is_student(
            first_name='Test',
            last_name='Guy',
            address_1='1273 Dillon Rd',
            email='test@guy.com',
        )
        self._check_api_output(student)

    def test_missing_name(self):
        """
        Not specifying a first or last name isn't allowed.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.is_student,
                email='yarrr@yarrrrr.com',
        )

    def test_not_student(self):
        """
        Ask for a student that doesn't exist to test the failure condition.
        """
        student = self.api.is_student(
            first_name='Marty',
            last_name='Boogerpants',
            email='yarrr@yarrrrr.com',
        )
        self.assertFalse(student)


class TestGetStudent(unittest.TestCase):
    """
    Tests LigonierAPI.get_student().
    """

    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, student):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(student.AccountNumber, long)
        test_util.assertIsInstance(student.RecordId, long)
        test_util.assertIsInstance(student.DocumentNumber, (NoneType, long))
        test_util.assertIsInstance(student.AddressId, (NoneType, long))
        test_util.assertIsInstance(student.SourceCode,
            (NoneType, basestring))
        test_util.assertIsInstance(student.PartnerStatus, bool)
        test_util.assertIsInstance(student.FirstName, (NoneType, basestring))
        test_util.assertIsInstance(student.LastName, (NoneType, basestring))
        test_util.assertIsInstance(student.Email, basestring)
        test_util.assertIsInstance(student.PhoneNumber,
            (NoneType, basestring))
        test_util.assertIsInstance(student.Title, basestring)
        test_util.assertIsInstance(student.AccountType, basestring)
        test_util.assertIsInstance(student.Active, bool)
        test_util.assertIsInstance(student.ErrorMessage,
            (NoneType, basestring))

    def test_basic(self):
        """
        Nothing special here, just make sure it's working with normal
        valid values.
        """

        account = _create_random_test_account(self.api)

        student1 = self.api.get_student(
            first_name='Test',
            last_name='Guy',
            account_number=account.AccountNumber,
        )
        self._check_api_output(student1)

        # Now omit first/last name, since they probably aren't needed.
        student2 = self.api.get_student(
            account_number=account.AccountNumber,
        )
        self._check_api_output(student2)

    def test_invalid_account_number(self):
        """
        Pass some bad values to verify validation.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.get_student,
                account_number=-1,
        )

        self.assertRaises(APIInvalidInputException,
            self.api.get_student,
                account_number='a',
        )

    def test_not_student(self):
        """
        Ask for a student that doesn't exist to test the failure condition.
        """
        self.assertRaises(StudentNotFoundException,
            self.api.get_student,
                account_number=999999999999999999,
        )


class TestGetStudentEmailList(unittest.TestCase):
    """
    Tests LigonierAPI.get_student_email_list().
    """

    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, address):
        """
        Checks output for validity.
        """

        test_util.assertIsInstance(address['record_id'], long)
        test_util.assertIsInstance(address['account_number'], long)
        test_util.assertIsInstance(address['email_type'], basestring)
        test_util.assertIsInstance(address['email_address'], basestring)
        test_util.assertIsInstance(address['use_as_primary'], bool)
        test_util.assertIsInstance(address['active_email'], bool)

    def test_basic(self):
        """
        Nothing special here, just make sure it's working with normal
        valid values.
        """

        account = _create_random_test_account(self.api)

        emails = self.api.get_student_email_list(
            account_number=account.AccountNumber,
        )
        for email in emails:
            self._check_api_output(email)

    def test_invalid_account_number(self):
        """
        Pass some bad values to verify validation.
        """

        self.assertRaises(APIInvalidInputException,
            self.api.get_student_email_list,
            account_number=-1,
        )

        self.assertRaises(APIInvalidInputException,
            self.api.get_student_email_list,
            account_number='a',
        )

    def test_not_student(self):
        """
        Ask for a student that doesn't exist to test the failure condition.
        """

        self.assertRaises(StudentNotFoundException,
            self.api.get_student_email_list,
            account_number=8,
        )


class TestGetStudentPhoneList(unittest.TestCase):
    """
    Tests LigonierAPI.get_student_phone_list().
    """

    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, address):
        """
        Checks output for validity.
        """

        test_util.assertIsInstance(address['record_id'], long)
        test_util.assertIsInstance(address['account_number'], long)
        test_util.assertIsInstance(address['phone_type'], basestring)
        test_util.assertIsInstance(address['international_code'], (NoneType, basestring))
        test_util.assertIsInstance(address['area_code'], basestring)
        test_util.assertIsInstance(address['phone_number'], basestring)
        test_util.assertIsInstance(address['extension'], (NoneType, basestring))
        test_util.assertIsInstance(address['use_as_primary'], bool)
        test_util.assertIsInstance(address['active_phone'], bool)

    def test_basic(self):
        """
        Nothing special here, just make sure it's working with normal
        valid values.
        """

        account = _create_random_test_account(self.api)

        phones = self.api.get_student_phone_list(
            account_number=account.AccountNumber,
        )
        for phone in phones:
            self._check_api_output(phone)

    def test_invalid_account_number(self):
        """
        Pass some bad values to verify validation.
        """

        self.assertRaises(APIInvalidInputException,
            self.api.get_student_phone_list,
            account_number=-1,
        )

        self.assertRaises(APIInvalidInputException,
            self.api.get_student_phone_list,
            account_number='a',
        )

    def test_not_student(self):
        """
        Ask for a student that doesn't exist to test the failure condition.
        """

        self.assertRaises(StudentNotFoundException,
            self.api.get_student_phone_list,
            account_number=8,
        )


class TestGetStudentAddressList(unittest.TestCase):
    """
    Tests LigonierAPI.get_student_address_list().
    """

    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, address):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(address['address_line_1'], basestring)
        test_util.assertIsInstance(address['address_line_2'], (basestring, NoneType))
        test_util.assertIsInstance(address['city'], basestring)
        test_util.assertIsInstance(address['state'], basestring)
        test_util.assertIsInstance(address['zip'], basestring)
        test_util.assertIsInstance(address['address_type'], (basestring, NoneType))
        test_util.assertIsInstance(address['plus4_code'], (basestring, NoneType))
        test_util.assertIsInstance(address['country'], (basestring, NoneType))
        test_util.assertIsInstance(address['address_id'], long)
        test_util.assertIsInstance(address['record_id'], long)

    def test_basic(self):
        """
        Nothing special here, just make sure it's working with normal
        valid values.
        """

        account = _create_random_test_account(self.api)

        addresses = self.api.get_student_address_list(
            account_number=account.AccountNumber,
        )
        for address in addresses:
            self._check_api_output(address)

    def test_invalid_account_number(self):
        """
        Pass some bad values to verify validation.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.get_student_address_list,
            account_number=-1,
        )

        self.assertRaises(APIInvalidInputException,
            self.api.get_student_address_list,
            account_number='a',
        )

    def test_not_student(self):
        """
        Ask for a student that doesn't exist to test the failure condition.
        """
        self.assertRaises(StudentNotFoundException,
            self.api.get_student_address_list,
            account_number=8,
        )


class TestGetStudentAddress(unittest.TestCase):
    """
    Tests LigonierAPI.get_student_address().
    """

    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, address):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(address.AddressLine1, basestring)
        test_util.assertIsInstance(address.AddressLine2, (basestring, NoneType))
        test_util.assertIsInstance(address.City, basestring)
        test_util.assertIsInstance(address.State, basestring)
        test_util.assertIsInstance(address.Zip, basestring)
        test_util.assertIsInstance(address.AddressType, (basestring, NoneType))
        test_util.assertIsInstance(address.AddressErrorString, (NoneType, basestring))
        test_util.assertIsInstance(address.Plus4Code, (basestring, NoneType))
        test_util.assertIsInstance(address.Country, (basestring, NoneType))
        test_util.assertIsInstance(address.AddressId, long)
        test_util.assertIsInstance(address.RecordId, long)
        test_util.assertIsInstance(address.ErrorCode, basestring)
        test_util.assertIsInstance(address.ErrorMessage,
            (basestring, NoneType))

    def test_basic(self):
        """
        Nothing special here, just make sure it's working with normal
        valid values.
        """

        # This creates an account with ship address line 1 being 1274 Dillon Rd,
        # and bill address line 1 being 1273 Dillon Rd. This disparity lets us
        # make sure getStudentAddress is working as intended.
        account = _create_random_test_account(self.api)

        address = self.api.get_student_address(
            account_number=account.AccountNumber,
            # Retrieve the address that was created by ISDR for shipping.
            address_1=account.ShipStudentAddress.AddressLine1,
        )
        self._check_api_output(address)
        # The returned address should be equal to the one we specified as
        # shipping address on ISDR (1274 Dillon Rd).
        self.assertEqual(address.AddressLine1, account.ShipStudentAddress.AddressLine1)

        address = self.api.get_student_address(
            account_number=account.AccountNumber,
            # Now see if we can retreive the address used as billin with ISDR.
            address_1=account.BillStudentAddress.AddressLine1,
        )
        self._check_api_output(address)
        # The returned address should be equal to the one we specified as
        # billing address on ISDR (1273 Dillon Rd).
        self.assertEqual(address.AddressLine1, account.BillStudentAddress.AddressLine1)

        # Now omit all optional args.
        address = self.api.get_student_address(
            account_number=account.AccountNumber,
        )
        self._check_api_output(address)
        # Since no address_1 was specified, the default is to return the
        # address marked as billing.
        self.assertEqual(address.AddressLine1, account.BillStudentAddress.AddressLine1)

        # Now give it a bogus address and see what happens.
        # Now omit all optional args.
        # TODO: Re-add this when we figure out how to handle it.
        #address = self.api.get_student_address(
        #    account_number=account.AccountNumber,
        #    address_1='Blarty blartfast'
        #)

    def test_invalid_account_number(self):
        """
        Pass some bad values to verify validation.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.get_student_address,
                account_number=-1,
        )

        self.assertRaises(APIInvalidInputException,
            self.api.get_student_address,
                account_number='a',
        )

    def test_not_student(self):
        """
        Ask for a student that doesn't exist to test the failure condition.
        """
        self.assertRaises(StudentNotFoundException,
            self.api.get_student_address,
                account_number=8,
        )


class TestGetStudentYearEndStatement(unittest.TestCase):
    """
    Tests LigonierAPI.get_student_year_end_statement().
    """

    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, student):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(student.AccountNumber, long)
        test_util.assertIsInstance(student.RecordId, long)
        test_util.assertIsInstance(student.DocumentNumber, (NoneType, long))
        test_util.assertIsInstance(student.AddressId, (NoneType, long))
        test_util.assertIsInstance(student.SourceCode,
            (NoneType, basestring))
        test_util.assertIsInstance(student.FirstName, basestring)
        test_util.assertIsInstance(student.PartnerStatus, bool)
        test_util.assertIsInstance(student.LastName, basestring)
        test_util.assertIsInstance(student.Email, basestring)
        test_util.assertIsInstance(student.PhoneNumber,
            (NoneType, basestring))
        test_util.assertIsInstance(student.Title, basestring)
        test_util.assertIsInstance(student.AccountType, basestring)
        test_util.assertIsInstance(student.Active, bool)
        test_util.assertIsInstance(student.ErrorMessage,
            (NoneType, basestring))

    def test_basic(self):
        """
        Nothing special here, just make sure it's working with normal
        valid values.
        """
        pass
        #statement1 = self.api.get_student_year_end_statement(
        #    account_number=123456,
        #    year=2011,
        #)

    def test_invalid_account_number(self):
        """
        Pass some bad values to verify validation.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.get_student_year_end_statement,
                account_number=-1,
                year=2011,
        )

        self.assertRaises(APIInvalidInputException,
            self.api.get_student_year_end_statement,
                account_number='a',
                year=2011,
        )

    def test_not_student(self):
        """
        Ask for a student that doesn't exist to test the failure condition.
        """
        pass
        #self.assertRaises(StudentNotFoundException,
        #    self.api.get_student_year_end_statement,
        #        account_number=8,
        #        year=2011,
        #)