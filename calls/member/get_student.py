from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import StudentNotFoundException

class GetStudent(BaseAPICall):
    """
    API call for retrieving student information, in the case where the
    account number is known.

    Last known signature in the WSDL::

        getStudent(
            xs:string key,
            xs:string firstName,
            xs:string lastName,
            xs:string accountNumber,
        )
    """

    @property
    def service_method(self):
        return self.api.get_service.getStudent

    def _pre_process(self):
        """
        Perform some validation.
        """
        self._validate_account_number()

    def _post_process(self, response):
        """
        Checks the response to see if there is or isn't an account number
        that is equal to ``None``. No account number means no match.

        :raises: StudentNotFoundException when no account number match
            could be found.
        """
        if response.AccountNumber in [0, None]:
            # No match.
            raise StudentNotFoundException(
                'No student found with account number: %s' % self.kwargs.get(
                    'accountNumber')
            )

        response.PartnerStatus = response.PartnerStatus == 'MP'

        if not hasattr(response, 'Title'):
            response.Title = 'Mr.'

        if not hasattr(response, 'FirstName'):
            response.FirstName = None

        if not hasattr(response, 'LastName'):
            response.LastName = None

        return response
