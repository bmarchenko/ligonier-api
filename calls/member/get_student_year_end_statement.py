from ligonier_api.calls.base import BaseAPICall


class GetStudentYearEndStatement(BaseAPICall):
    """
    API call for retrieving a year-end statement PDF for a student.

    Last known signature in the WSDL::

        getStudentYearEndStatement(
            xs:string key,
            xs:string accountNumber,
            xs:long year,
        )
    """
    # Don't log the PDF output.
    supress_response_body_logging = True

    @property
    def service_method(self):
        return self.api.get_service.getStudentYearEndStatement

    def _pre_process(self):
        """
        Perform some validation.
        """
        self._validate_account_number()

        if not self.kwargs.get('year'):
            self.kwargs['year'] = str(self.kwargs.get('year')).encode('utf-8')