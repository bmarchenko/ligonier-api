from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import StudentNotFoundException

class GetStudentPhoneList(BaseAPICall):
    """
    API call for retrieving student phone numbers, in the case where the
    account number is known.

    Last known signature in the WSDL::

        getStudentPhone(
            xs:string key,
            xs:string accountNumber,
        )
    """

    @property
    def service_method(self):
        return self.api.get_service.getStudentPhone

    def _pre_process(self):
        """
        Perform some validation.
        """
        self._validate_account_number()

    def _post_process(self, response):
        """
        Checks the response to see if there is or isn't an account number
        that is equal to ``None``. No account number means no match.

        :raises: StudentNotFoundException when no account number match
            could be found.
        """

        phones = []

        for phone in response.StudentPhone:
            if getattr(phone, 'ErrorCode', None) == '-1000':
                raise StudentNotFoundException(
                    'No student found with account number: %s' % self.kwargs.get(
                        'accountNumber')
                )
            phones.append(
                {
                    'record_id': phone.RecordId,
                    'account_number': phone.AccountNumber,
                    'phone_type': phone.PhoneType,
                    'international_code': phone.InternationCode,
                    'area_code': phone.AreaCode,
                    'phone_number': phone.PhoneNumber,
                    'extension': phone.Extension,
                    'use_as_primary': phone.UseAsPrimary == 1,
                    'active_phone': phone.Active == 1,
                }
            )

        return phones
