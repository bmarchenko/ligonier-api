import datetime
from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import APIErrorFromServiceException, StudentNotFoundException

class GetStudentOrderSummary(BaseAPICall):
    """
    Retrieves a student's order history.

    Last known signature in the WSDL::

        getOrderSummary(
            xs:long accountNumber,
            xs:int daysBack,
        )
    """

    @property
    def service_method(self):
        return self.api.get_service.getOrderSummary

    def _pre_process(self):
        """
        Perform some validation.
        """
        self._validate_account_number()

    def _post_process(self, response):
        """
        Checks for the presence of the ``ErrorMessage`` attrib on StudentDetail.

        :raises: APIErrorFromServiceException if an error was returned.
        """
        error_code = getattr(response.OrderSummary[0], 'ErrorCode', False)
        if error_code:
            if error_code == '-1000':
                # Either the student had no records, or an invalid student
                # number was passed. The middleware does nothing to figure
                # out which case this is, so we'll just fail silently.
                return []
            else:
                APIErrorFromServiceException(
                    response.OrderSummary[0].ErrorMessage
                )

        orders = []

        for order in response.OrderSummary:
            order_date = getattr(order, 'OrderDate', None)
            if not order_date:
                # We can't really do anything without an order date.
                continue

            # This is sometimes inexplicably completely absent from the response.
            ship_date = getattr(order, 'ShipDate', None)

            if not ship_date:
                order.ShipDate = None
            else:
                # Empty string ('') evaluates to False, whereas a single
                # space (' ') evaluates to true. Prevent against garbage.
                ship_date = ship_date.strip()

                parsed_date = datetime.datetime.strptime(ship_date, '%m/%d/%Y')
                new_date = datetime.date(
                    parsed_date.year,
                    parsed_date.month,
                    parsed_date.day,
                )
                order.ShipDate = new_date

            parsed_date = datetime.datetime.strptime(order.OrderDate, '%m/%d/%Y')
            new_date = datetime.date(
                parsed_date.year,
                parsed_date.month,
                parsed_date.day,
            )
            order.OrderDate = new_date

            if order.GPsop:
                order.GPsop = order.GPsop.strip()

            orders.append(order)

        return orders