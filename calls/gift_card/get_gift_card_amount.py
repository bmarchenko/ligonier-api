from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import APIErrorFromServiceException, APIInvalidInputException, GiftCardNotFoundException

class GetGiftCardAmount(BaseAPICall):
    """
    API call for checking the balance on a gift card..

    Last known signature in the WSDL::

        getGiftCardAmount(
            xs:int giftCardNumber,
            xs:int giftCardKey,
        )
    """
    @property
    def service_method(self):
        return self.api.get_service.getGiftCardAmount

    def _pre_process(self):
        """
        Perform some validation.
        """
        self._validate_account_number(attrib_name='giftCardNumber')
        self._validate_account_number(attrib_name='giftCardKey')

    def _post_process(self, response):
        """
        Checks for the presence of the ``ErrorMessage`` attrib on the response.

        :raises: APIErrorFromServiceException if an error was returned.
            InsufficientGiftCardFunds if the value for ``amount`` is higher than
            the remaining balance on the gift card.
        """

        error_code = getattr(response, 'ErrorCode', None)
        if error_code == '-1000':
            raise GiftCardNotFoundException(
                'Gift card #%s with key %s not found.' % (
                    self.kwargs.get('giftCardNumber'),
                    self.kwargs.get('giftCardKey'),
                )
            )
        elif error_code:
            error_msg = response.ErrorMessage
            raise APIErrorFromServiceException(
                '%s (Error Code: %s)' % (error_msg, error_code),
            )

        return self.clean_dollar_amount(response.Amount)