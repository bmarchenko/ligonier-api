from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import APIErrorFromServiceException, APIInvalidInputException


class DoCreateGiftCardTransaction(BaseAPICall):
    """
    API call for conducting a giftcard creation transaction.

    Last known signature in the WSDL::

        CreateGiftCard(
            xs:long accountNumber,
            xs:int value,
        )
    """
    @property
    def service_method(self):
        return self.api.set_service.CreateGiftCard

    def _pre_process(self):
        """
        Perform some validation.
        """
        #noinspection PyUnresolvedReferences
        self._validate_account_number()

        card_value = self.kwargs.get('value')
        # Validate it! Normally we'd use self._validate_dollar_amount, but the
        # value here is an int instead of a float/Decimal.
        try:
            card_value = int(card_value)
            if card_value < 0:
                raise APIInvalidInputException("Card value can't be negative.")
        except (TypeError, ValueError):
            raise APIInvalidInputException(
                'Invalid gift card amount: %s' % card_value
            )

        if not self.kwargs.get('email'):
            self.kwargs['email'] = ''

    def _post_process(self, response):
        """
        Checks for the presence of the ``ErrorMessage`` attrib on the response.

        :raises: APIErrorFromServiceException if an error was returned.
        """
        error_msg = getattr(response, 'ErrorMessage', None)
        if error_msg:
            # ErrorMessage key was present on the TransactionDetail object
            # returned from the service. This is only present when something bad
            # happens, so raise an exception.
            raise APIErrorFromServiceException(
                '%s (Error Code: %s)' % (error_msg, response.ErrorCode),
            )

        return response