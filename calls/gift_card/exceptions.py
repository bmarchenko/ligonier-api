"""
Exceptions that are specific to gift cards.
"""
from ligonier_api.exceptions import APIException

class InsufficientGiftCardFunds(APIException):
    """
    Raised when a user attempts to apply a gift card to a transaction, but
    has insufficient funds to cover the specified amount.
    """
    pass
