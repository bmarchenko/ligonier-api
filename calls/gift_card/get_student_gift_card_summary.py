from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import APIErrorFromServiceException, StudentNotFoundException

class GetStudentGiftCardSummary(BaseAPICall):
    """
    API call for conducting an donate transaction.

    Last known signature in the WSDL::

        getStudentGiftCard(
            xs:long accountNumber,
        )
    """

    @property
    def service_method(self):
        return self.api.get_service.getStudentGiftCard

    def _pre_process(self):
        """
        Perform some validation.
        """

        self._validate_account_number()

    def _post_process(self, response):
        """
        Checks for the presence of the ``ErrorMessage`` attrib on the response.

        :raises: APIErrorFromServiceException if an error was returned.
            InsufficientGiftCardFunds if the value for ``amount`` is higher than
            the remaining balance on the gift card.
        """

        for entry in response.GiftCard:
            # If any values were returned, check the first one of errors.
            error_code = getattr(entry, 'ErrorCode', None)
            if error_code in ['-1000', '-1002']:
                raise StudentNotFoundException(
                    'No student found with account number: %s' % self.kwargs.get(
                        'accountNumber')
                )
            elif error_code:
                error_msg = entry.ErrorMessage
                raise APIErrorFromServiceException(
                    '%s (Error Code: %s)' % (error_msg, error_code),
                )

            self.cast_attr_to_dollars(entry, 'Amount')

        return response.GiftCard