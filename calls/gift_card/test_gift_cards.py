from decimal import Decimal
import unittest
from types import NoneType

from ligonier_api.tests import test_util, interface_factory, test_defines
from ligonier_api.exceptions import APIInvalidInputException, APIErrorFromServiceException, StudentNotFoundException, GiftCardNotFoundException
from ligonier_api.tests.test_util import _create_random_test_account

class TestDoCreateGiftCardTransaction(unittest.TestCase):
    """
    Tests LigonierAPI.do_create_gift_card_transaction().
    """
    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, transaction):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(transaction.GiftCardId, int)
        test_util.assertIsInstance(transaction.GiftCardNumber, int)
        test_util.assertIsInstance(transaction.GiftCardKey, int)
        test_util.assertIsInstance(transaction.Value, int)
        test_util.assertIsInstance(transaction.Amount, float)
        test_util.assertIsInstance(transaction.AccountNumber, long)
        test_util.assertIsInstance(transaction.DocumentNumber, long)
        test_util.assertIsInstance(transaction.Email, (basestring, NoneType))
        test_util.assertIsInstance(transaction.ErrorCode,
            (basestring, NoneType))
        test_util.assertIsInstance(transaction.ErrorMessage,
            (basestring, NoneType))

    def test_basic(self):
        """
        Test the default, 'send everything' behavior.
        """

        account = _create_random_test_account(self.api)

        result = self.api.do_create_gift_card_transaction(
            account_number=account.AccountNumber,
            value=10,
        )
        self._check_api_output(result)

    def test_omit_account_num(self):
        """
        Intentionally omit account number. Should raise an exception.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.do_create_gift_card_transaction,
                # Omitted.
                account_number='',
                value=10,
        )

    def test_negative_amount(self):
        """
        Test sending a negative amount.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.do_create_gift_card_transaction,
                account_number=12345678,
                # Negative
                value=-10,
        )

    def test_omitted_amount(self):
        """
        Test omitting an amount.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.do_create_gift_card_transaction,
                account_number=1234567,
                # Omitted.
                value=None,
        )


class TestDoGiftCardTransaction(unittest.TestCase):
    """
    Tests LigonierAPI.do_gift_card_transaction().
    """
    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, transaction):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(transaction.GiftCardId, int)
        test_util.assertIsInstance(transaction.GiftCardNumber, int)
        test_util.assertIsInstance(transaction.GiftCardKey, int)
        test_util.assertIsInstance(transaction.Value, int)
        test_util.assertIsInstance(transaction.Amount, Decimal)
        test_util.assertIsInstance(transaction.AccountNumber, long)
        test_util.assertIsInstance(transaction.DocumentNumber, long)
        #test_util.assertIsInstance(transaction.Email, (basestring, NoneType))
        test_util.assertIsInstance(transaction.ErrorCode,
            (basestring, NoneType))
        test_util.assertIsInstance(transaction.ErrorMessage,
            (basestring, NoneType))

    def test_basic(self):
        """
        Test the default, 'send everything' behavior.
        """

        account = _create_random_test_account(self.api)

        card_trans = self.api.do_create_gift_card_transaction(
            account_number=account.AccountNumber,
            value=12,
        )

        #print card_trans
        card_number = card_trans.GiftCardNumber
        card_key = card_trans.GiftCardKey

        result = self.api.do_gift_card_transaction(
            account_number=account.AccountNumber,
            order_document_number=1,
            gift_card_number=card_number,
            gift_card_key=card_key,
            amount=3.3,
        )
        #print result
        self._check_api_output(result)

        result = self.api.get_gift_card_amount(
            gift_card_number=card_number,
            gift_card_key=card_key,
        )
        self.assertEqual(result, Decimal('8.70'))

    def test_omit_card_num(self):
        """
        Intentionally omit account number. Should raise an exception.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.do_gift_card_transaction,
                account_number=1234567,
                order_document_number=1,
                # Omitted.
                gift_card_number=None,
                gift_card_key=1,
                amount=1.0,
        )

    def test_invalid_card_nums(self):
        """
        Intentionally pass bogus card numbers. Should raise an exception.
        """
        self.assertRaises(GiftCardNotFoundException,
            self.api.do_gift_card_transaction,
                account_number=1234567,
                order_document_number=1,
                # Invalid
                gift_card_number=123,
                gift_card_key=10,
                amount=1.0,
        )

    def test_negative_amount(self):
        """
        Test sending a negative amount.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.do_gift_card_transaction,
                order_document_number=1,
                account_number=1234567,
                gift_card_number=1,
                gift_card_key=1,
                # Negative
                amount=-10.0,
        )

    def test_omitted_amount(self):
        """
        Test omitting an amount.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.do_gift_card_transaction,
                account_number=1234567,
                order_document_number=1,
                gift_card_number=1,
                gift_card_key=1,
                # Omitted
                amount=None,
        )


class TestGetStudentGiftCardSummary(unittest.TestCase):
    """
    Tests LigonierAPI.get_student_gift_card_summary().
    """
    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, transaction):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(transaction.GiftCardId, int)
        test_util.assertIsInstance(transaction.GiftCardNumber, int)
        test_util.assertIsInstance(transaction.GiftCardKey, int)
        test_util.assertIsInstance(transaction.Value, int)
        test_util.assertIsInstance(transaction.Amount, Decimal)
        test_util.assertIsInstance(transaction.AccountNumber, long)
        test_util.assertIsInstance(transaction.DocumentNumber, long)
        test_util.assertIsInstance(transaction.Email, (basestring, NoneType))

    def test_basic(self):
        """
        Test the default, 'send everything' behavior.
        """

        account = _create_random_test_account(self.api)

        self.api.do_create_gift_card_transaction(
            account_number=account.AccountNumber,
            value=10,
        )

        result = self.api.get_student_gift_card_summary(
            account_number=account.AccountNumber,
        )
        for card in result:
            self._check_api_output(card)

    def test_omit_account_num(self):
        """
        Intentionally omit account number. Should raise an exception.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.get_student_gift_card_summary,
                # Omitted.
                account_number='',
        )

    def test_invalid_account_num(self):
        self.assertRaises(StudentNotFoundException,
            self.api.get_student_gift_card_summary,
                # Invalid.
                account_number=1,
        )


class TestGetGiftCardAmount(unittest.TestCase):
    """
    Tests LigonierAPI.get_gift_card_amount().
    """

    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, transaction):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(transaction.GiftCardId, int)
        test_util.assertIsInstance(transaction.GiftCardNumber, int)
        test_util.assertIsInstance(transaction.GiftCardKey, int)
        test_util.assertIsInstance(transaction.Value, int)
        test_util.assertIsInstance(transaction.Amount, Decimal)
        test_util.assertIsInstance(transaction.AccountNumber, long)
        test_util.assertIsInstance(transaction.DocumentNumber, long)
        test_util.assertIsInstance(transaction.Email, (basestring, NoneType))

    def test_basic(self):
        """
        Test the default, 'send everything' behavior.
        """

        account = _create_random_test_account(self.api)

        card_trans = self.api.do_create_gift_card_transaction(
            account_number=account.AccountNumber,
            value=10,
            email=account.Email,
        )

        card_number = card_trans.GiftCardNumber
        card_key = card_trans.GiftCardKey

        result = self.api.get_gift_card_amount(
            gift_card_number=card_number,
            gift_card_key=card_key,
        )
        self.assertEqual(result, 10.0)

    def test_omit_card_nums(self):
        """
        Intentionally omit account number. Should raise an exception.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.get_gift_card_amount,
                # Omitted.
                gift_card_number='',
                gift_card_key=1,
        )

        self.assertRaises(APIInvalidInputException,
            self.api.get_gift_card_amount,
                gift_card_number=1,
                # Omitted.
                gift_card_key='',
        )

    def test_invalid_card_num(self):
        self.assertRaises(GiftCardNotFoundException,
            self.api.get_gift_card_amount,
                gift_card_number=1,
                # Omitted.
                gift_card_key=1,
        )
