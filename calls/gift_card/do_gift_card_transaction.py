from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import APIErrorFromServiceException, APIInvalidInputException, GiftCardNotFoundException
from ligonier_api.calls.gift_card.exceptions import InsufficientGiftCardFunds

class DoGiftCardTransaction(BaseAPICall):
    """
    API call for conducting an donate transaction.

    Last known signature in the WSDL::

        DoGiftCardTransaction(
            xs:int giftCardNumber,
            xs:int giftCardKey,
            xs:decimal amount,
            xs:long documentNumber,
            xs:long accountNumber,
        )
    """
    @property
    def service_method(self):
        return self.api.set_service.GiftCardTransaction

    def _pre_process(self):
        """
        Perform some validation.
        """
        self._validate_account_number()
        self._validate_account_number(attrib_name='giftCardNumber')
        self._validate_dollar_amount('amount')

        document_number = str(self.kwargs.get('documentNumber'))
        if not document_number:
            raise APIInvalidInputException(
                'No order_document_number value provided.'
            )

        if 'WEB' in document_number:
            # This is a temporary change to get us through the old and new
            # API co-existence phase. Remove this when we get to DS deployment.
            self.kwargs['documentNumber'] = int(document_number.replace('WEB', ''))

    def _post_process(self, response):
        """
        Checks for the presence of the ``ErrorMessage`` attrib on the response.

        :raises: APIErrorFromServiceException if an error was returned.
            InsufficientGiftCardFunds if the value for ``amount`` is higher than
            the remaining balance on the gift card.
        """

        error_code = getattr(response, 'ErrorCode', None)
        if error_code in ['-1000', '-1002', '-1003', '-1008']:
            raise GiftCardNotFoundException(
                'Gift card #%s with key %s not found.' % (
                    self.kwargs.get('giftCardNumber'),
                    self.kwargs.get('giftCardKey'),
                    )
            )
        elif error_code == '-1007':
            raise InsufficientGiftCardFunds(
                'Unable to debit gift card %s for $%s. Insufficient funds.' % (
                    self.kwargs.get('giftCardNumber'),
                    self.kwargs.get('amount'),
                )
            )
        elif error_code:
            error_msg = response.ErrorMessage
            raise APIErrorFromServiceException(
                '%s (Error Code: %s)' % (error_msg, error_code),
            )

        self.cast_attr_to_dollars(response, 'Amount')

        return response