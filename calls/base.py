"""
This module contains :py:class:`BaseAPICall`, which is the parent of all API
calls.
"""

from decimal import Decimal, InvalidOperation
import time
import logging
import pprint
from httplib import BadStatusLine
from ligonier_api.exceptions import APIInvalidInputException

_request_logger = logging.getLogger('ligonier_api.request')
_response_logger = logging.getLogger('ligonier_api.response')


class BaseAPICall(object):
    """
    A base API call class. All API calls inherit this. Implements a standard
    interface and course of action for API calls. Offers convenience methods
    for commonly performed actions. Also keeps the specifics of
    each call out of the top-level ligonier_api.api,
    which could get quite large.

    :attr LigonierAPI api: Reference to the root-level API object. This is where
        the suds gateway resides.
    :attr dict kwargs: The arguments passed to the API. These names match up
        with what is in the WSDL, and are not the pythonic names the API
        client presents.
    :attr bool supress_response_body_logging: If ``True``, don't log the
        response body coming back from the middleware. This is crucial for
        calls that return files (like PDFs), as we don't want to be printing
        that out.
    """
    supress_response_body_logging = False

    def __init__(self, api, **kwargs):
        """
        The ``kwargs`` will vary wildly based on which API call we're
        doing. These map directly to the argument names in the WSDL.

        :param LigonierAPI api: Reference to the root-level API object.
            This is where the suds gateway resides.
        """
        self.api = api
        self.kwargs = kwargs
        self.kwargs['key'] = self.api.api_key

    #noinspection PyPropertyDefinition
    @property
    def service_method(self):
        """
        Returns the SOAP service method to use for this API call.This is from
        the service's client object on the top-level LigonierAPI instance.

        .. warning:: This method needs to be overridden on each BaseAPICall
            sub-class

        :returns: A reference to the SOAP service's method to use for this
            call.
        """
        raise NotImplementedError(
            "Override _get_service_method on your BaseAPICall sub-class."
        )

    def _pre_process(self):
        """
        Perform validation and pre-processing on the data provided by the
        user. Raise any exceptions if we know incoming data to be bad without
        having to hit the API first. Convert any data types that we need to
        in order to keep the API server happy.
        """
        pass

    def _post_process(self, response):
        """
        Go over the data from the API and validate/convert things as needed.
        For example, convert date/time strings to Python datetime objects.
        If an error is detected in the response, this is where you'd raise
        the appropriate Exception sub-class for it.

        :param response: The response from the SOAP API server, as parsed
            by suds.
        :returns: The post-processed response.
        """
        return response

    def _validate_account_number(self, attrib_name='accountNumber',
                                 is_required=True):
        """
        Validates the account number passed to the call. This does not apply
        to all API calls.

        :keyword str attrib_name: Allows for checking an attribute besides
            the default 'accountNumber' for validity.
        :keyword bool is_required: If this is ``True``, an account number is
            required. If ``False``, an account number is optional.

        :raises: APIInvalidInputException if an invalid account number
            was found, or if an account number was required but not present.
        """
        account_number = self.kwargs.get(attrib_name)

        if account_number:
            try:
                account_number = int(account_number)
            except ValueError:
                raise APIInvalidInputException(
                    'Non-numerical account #: %s' % account_number
                )

            if account_number <= 0:
                raise APIInvalidInputException(
                    'Invalid account #: %s' % account_number
                )
        else:
            if is_required:
                raise APIInvalidInputException(
                    'No account # specified.'
                )

    def clean_dollar_amount(self, dollar_amount):
        """
        Given some kind of number, clean it up, validate it, then quantize
        it as money.

        :param dollar_amount: An int, float, or Decimal.
        :rtype: decimal.Decimal
        :return: A cleaned/validated Decimal instance.
        """
        if isinstance(dollar_amount, Decimal):
            # Already golden.
            pass
        elif isinstance(dollar_amount, int):
            # Int to Decimal is a straight shot.
            dollar_amount = Decimal(dollar_amount)
        elif isinstance(dollar_amount, float):
            # Lop it. Sadface.
            dollar_amount = Decimal('%.2f' % dollar_amount)
        elif isinstance(dollar_amount, basestring):
            # It's a string, do our best.
            try:
                dollar_amount = Decimal(dollar_amount)
            except InvalidOperation:
                raise APIInvalidInputException(
                    "Invalid dollar amount: %s" % dollar_amount
                )
        else:
            raise APIInvalidInputException(
                "Invalid dollar amount: %s" % dollar_amount,
            )

        if dollar_amount < Decimal('0.0'):
            raise APIInvalidInputException(
                "Negative dollar amount: %s" % dollar_amount,
            )

        return dollar_amount.quantize(Decimal('0.01'))

    def cast_attr_to_dollars(self, obj, attrib):
        """
        This is primarily used for casting the floats in middleware responses
        to Decimal money amounts, but can be used in other ways.

        :param obj: The object containing the currency attrib. Probably
            a SOAP response.
        :param str attrib: The attribute name to cast/replace.
        """

        dollar_amount = getattr(obj, attrib)
        setattr(obj, attrib, self.clean_dollar_amount(dollar_amount))

    def _validate_dollar_amount(self, key_name):
        """
        Given a self.kwargs key name, validate the value set for it as a valid
        dollar amount. Also cast it as a Decimal if it's not already. The
        cleaned Decimal is set as the keyword it came from.

        :param str key_name: The key name in the self.kwargs dict to check
            for a valid dollar amount.
        :raises: :py:exc:`ligonier_api.exceptions.APIInvalidInputException` if
            an invalid dollar amount is provided.
        :rtype: decimal.Decimal
        """

        dollar_amount = self.kwargs.get(key_name)
        self.kwargs[key_name] = self.clean_dollar_amount(dollar_amount)

    def _zipcode_yank_plus4(self, zip_code):
        """
        Given a zip code, sanitize it by lopping off the plus4. A number of
        middleware calls don't like the plus4.
        """
        if not zip_code:
            return zip_code

        zip = str(zip_code).split('-', 1)[0]

        return zip

    def get_response(self):
        """
        This is the method that causes work to be done. The
        :py:class:`ligonier_api.api.LigonierAPI` class uses this to cause
        the chain of events that lead up to an API request, and the
        post-processing that happens afterwards.

        :returns: The processed response from the API server. This varies
            wildly based on which call is being ran.
        """
        # Check input, convert data types of input, raise exceptions if we
        # know any of the input to be bad.
        self._pre_process()
        utf8_vals = self.kwargs.copy()
        for key, val in utf8_vals.items():
            if isinstance(val, basestring):
                # Convert to unicode to avoid some annoying errors.
                utf8_vals[key] = val.encode('utf-8')

        try:
            _request_logger.debug(u'Request -- %s : %s' % (
                self.__class__.__name__,
                pprint.pformat(utf8_vals),
            ))
        except UnicodeDecodeError:
            # It's just not that important.
            pass

        # Naively track the time the call started.
        pre_request_time = time.time()

        # The middleware fails for all kinds of different reasons. The following
        # is a naive attempt at auto-retrying after certain  kinds of failure.
        retry_counter = 0
        # Limit on retries.
        max_retries = 0
        while True:
            try:
                response = self.service_method(**self.kwargs)
                break
            except BadStatusLine:
                if retry_counter < max_retries:
                    retry_counter += 1
                    continue
                else:
                    raise

        # Naively track the time the call returned.
        post_request_time = time.time()

        if not self.supress_response_body_logging:
            #noinspection PyUnboundLocalVariable
            _response_logger.debug(u'Response -- %s : %s' % (
                self.__class__.__name__,
                response,
            ))

        # Post-process and return the results of the API call.
        #noinspection PyUnboundLocalVariable
        processed_response = self._post_process(response)

        # This is very naive, but should be good enough.
        response_time = post_request_time - pre_request_time

        call_name = self.__class__.__name__
        _response_logger.debug('Response time for %s: %f' % (
            call_name,
            response_time
        ))

        if self.api.post_response_callback:
            self.api.post_response_callback(
                call_name,
                response_time
            )

        return processed_response