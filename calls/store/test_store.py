import unittest
from decimal import Decimal
from types import NoneType
from ligonier_api.calls.store.exceptions import GuestbookDuplicateError

from ligonier_api.tests import test_util, interface_factory, test_defines
from ligonier_api.exceptions import APIInvalidInputException, APIErrorFromServiceException, InvalidAppealException
from ligonier_api.tests.test_util import _create_random_test_account, do_bluefin_transaction

class TestGetTax(unittest.TestCase):
    """
    Tests LigonierAPI.verify_address().
    """
    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, tax):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(tax.TotalTax, Decimal)
        test_util.assertIsInstance(tax.SalesTax1, Decimal)
        test_util.assertIsInstance(tax.SalesTax2, Decimal)
        test_util.assertIsInstance(tax.SalesTax3, Decimal)
        test_util.assertIsInstance(tax.SalesTax4, Decimal)
        test_util.assertIsInstance(tax.SalesTax5, Decimal)
        test_util.assertIsInstance(tax.ErrorCode, (basestring, NoneType))
        test_util.assertIsInstance(tax.ErrorMessage, (basestring, NoneType))

    def test_basic(self):
        """
        Nothing special here, just make sure it's working with normal
        valid values.
        """
        # Georgia tax, subject to tax.
        tax_ga = self.api.get_tax(
            amount=Decimal('20.00'),
            zip_code=30168,
        )
        self.assertGreater(tax_ga.TotalTax, Decimal('0.00'))
        self._check_api_output(tax_ga)

        # Zip code in SC, not subject to tax.
        tax_sc = self.api.get_tax(
            amount=Decimal('20.00'),
            zip_code=29678,
        )
        self.assertEqual(tax_sc.TotalTax, Decimal('0.00'))

    def test_plus4(self):
        """
        Make sure we can pass in a zip code with plus4.
        """
        tax_va = self.api.get_tax(
            amount=Decimal('20.00'),
            zip_code='22042-3144',
        )
        self._check_api_output(tax_va)

    def test_weird_amount(self):
        """
        Tests what happens when weird values for amount are specified.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.get_tax,
                amount=Decimal('-300.00'),
                zip_code='30168'
        )

        self.assertRaises(APIInvalidInputException,
            self.api.get_tax,
                amount='a',
                zip_code='30168'
        )

    def test_weird_zip_code(self):
        """
        Tests what happens when weird values for zip code are specified.
        """
        weird_zip_tax = self.api.get_tax(
            amount=Decimal('20.00'),
            zip_code=30348923480932,
        )
        self.assertEqual(weird_zip_tax.TotalTax, Decimal('0.00'))

        # Pass a non-numeric zip code.
        self.assertRaises(APIInvalidInputException,
            self.api.get_tax,
                amount=Decimal('300.00'),
                zip_code='a'
        )


class TestGetShippingCost(unittest.TestCase):
    """
    Tests LigonierAPI.get_shipping_cost().
    """
    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, ship_amount):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(ship_amount, Decimal)
        self.assertGreaterEqual(ship_amount, Decimal('0.0'))

    def test_basic(self):
        """
        Nothing special here, just make sure it's working with normal
        valid values.
        """
        # String zip code.
        shipcost1 = self.api.get_shipping_cost(
            weight=30,
            zip_code='30168',
            shipping_method='UPS-2DA',
        )
        self._check_api_output(shipcost1)

        # Int zip code.
        shipcost2 = self.api.get_shipping_cost(
            weight=30,
            zip_code=30168,
            shipping_method='UPS-G',
        )
        self._check_api_output(shipcost2)

    def test_plus4(self):
        """
        Makes sure we can handle plus4 zipcodes.
        """
        shipcost1 = self.api.get_shipping_cost(
            weight=30,
            zip_code='22042-3144',
            shipping_method='UPS-2DA',
        )
        self._check_api_output(shipcost1)

    def test_weird_amount(self):
        """
        Tests what happens when weird values for amount are specified.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.get_shipping_cost,
                weight=-30,
                zip_code='30168',
                shipping_method='UPS-G',
        )

        self.assertRaises(APIInvalidInputException,
            self.api.get_shipping_cost,
                weight='a',
                zip_code='30168',
                shipping_method='UPS-G',
        )

    def test_weird_zip_code(self):
        """
        Tests what happens when weird values for zip code are specified.
        """
        # Pass a bogus zip code.
        fake_zip = self.api.get_shipping_cost(
            weight=30,
            zip_code=303874329088032943,
            shipping_method='UPS-G',
        )
        # If we don't know the zip code, assume shipping cost of $0.
        self.assertEqual(fake_zip, 0.0)

        fake_zip2 = self.api.get_shipping_cost(
            weight=30,
            zip_code='a',
            shipping_method='UPS-G',
        )
        self.assertEqual(fake_zip2, 0.0)

    def test_invalid_shipping_method(self):
        """
        Provide a bogus shipping_method.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.get_shipping_cost,
                weight=30,
                zip_code=30168,
                shipping_method='GREGPS',
        )

    def test_blank_zip_code(self):
        """
        Pass a blank zip code.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.get_shipping_cost,
                weight=30,
                zip_code='',
                shipping_method='UPS-G',
        )


class TestGetShippingOptions(unittest.TestCase):
    """
    Tests LigonierAPI.get_shipping_options().
    """

    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, opts):
        """
        Checks output for validity.
        """
        for opt in opts:
            test_util.assertIsInstance(opt.ShippingPrice, Decimal)
            test_util.assertIsInstance(opt.ShippingDescription, basestring)
            test_util.assertIsInstance(opt.Recommended, bool)
            test_util.assertIsInstance(opt.ShippingCode, basestring)

    def test_basic(self):
        """
        Nothing special here, just make sure it's working with normal
        valid values.
        """
        # Zip with plus4.
        shipopts1 = self.api.get_shipping_options(
            weight=30,
            zip_code='22042-3144',
        )
        self._check_api_output(shipopts1)

        # Int zip code.
        shipopts2 = self.api.get_shipping_options(
            weight=30,
            zip_code=32795,
        )
        self._check_api_output(shipopts2)

    def test_us_weight_over_70_lbs(self):
        """
        Greg F mentioned a 70 lbs weight limit on US shipments, test the
        behavior here.
        """
        shipopts1 = self.api.get_shipping_options(
            weight=100,
            zip_code='30168',
        )
        self._check_api_output(shipopts1)

    def test_weird_amount(self):
        """
        Tests what happens when weird values for amount are specified.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.get_shipping_options,
                weight=-30,
                zip_code='30168'
        )

        self.assertRaises(APIInvalidInputException,
            self.api.get_shipping_options,
                weight='a',
                zip_code='30168'
        )

    def test_weird_zip_code(self):
        """
        Tests what happens when weird values for zip code are specified.
        """
        # Pass a bogus zip code.
        self.assertRaises(APIErrorFromServiceException,
            self.api.get_shipping_options,
                weight=30,
                zip_code=3328904280940
        )

        # Pass a non-numeric zip code.
        self.assertRaises(APIErrorFromServiceException,
            self.api.get_shipping_options,
                weight=30,
                zip_code='a'
        )

    def test_blank_zip_code(self):
        """
        Pass a blank zip code.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.get_shipping_options,
                weight=30,
                zip_code=''
        )

    def test_international_zip_code(self):
        """
        Get shipping options for international shipping.
        """
        shipopts1 = self.api.get_shipping_options(
            weight=7.0,
            zip_code='int',
        )
        self._check_api_output(shipopts1)

class TestGetProducts(unittest.TestCase):
    """
    Tests LigonierAPI.get_products().
    """
    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, product):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(product.ItemNumber, basestring)
        test_util.assertIsInstance(product.ItemDescription, basestring)
        test_util.assertIsInstance(product.QuantityOnHand, int)
        self.assertGreaterEqual(product.QuantityOnHand, 0)
        test_util.assertIsInstance(product.ItemWeight, float)
        test_util.assertIsInstance(product.StandardPrice, float)
        test_util.assertIsInstance(product.BookStorePrice, float)
        test_util.assertIsInstance(product.ChurchPrice, float)
        test_util.assertIsInstance(product.UPC, (basestring, NoneType))
        test_util.assertIsInstance(product.ISBN, (basestring, NoneType))
        test_util.assertIsInstance(product.AllowBackOrder, bool)
        test_util.assertIsInstance(product.WebSite, bool)
        test_util.assertIsInstance(product.Taxable, bool)
        test_util.assertIsInstance(product.SourceCode, (basestring, NoneType))
        test_util.assertIsInstance(product.ErrorCode, basestring)
        test_util.assertIsInstance(product.ErrorMessage, (basestring, NoneType))

    def test_basic(self):
        """
        Test the default, 'send everything' behavior.
        """
        for product in self.api.get_products():
            self._check_api_output(product)

    def test_only_these(self):
        """
        Sanity check the output.
        """
        only_these = ['MOS03DC', 'REF42S']
        counter = 0
        for product in self.api.get_products(only_these=only_these):
            self._check_api_output(product)
            counter += 1
        # We should only have two results.
        self.assertEqual(counter, 2)

    def test_invalid_product(self):
        """
        Intentionally pass an invalid product. Should ignore the errant
        product codes, and only return matches.
        """
        only_these = ['INVALID', 'ORI01BP', 'SON02MC', 'BLARTY']
        counter = 0
        for product in self.api.get_products(only_these=only_these):
            self._check_api_output(product)
            counter += 1
        # We should only have two valid results.
        self.assertEqual(counter, 2)


class TestGetSourceCodeProducts(unittest.TestCase):
    """
    Tests LigonierAPI.get_source_code_products().
    """
    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, product):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(product.Description, basestring)
        test_util.assertIsInstance(product.ProductCode, basestring)
        test_util.assertIsInstance(product.SourceCode, basestring)
        test_util.assertIsInstance(product.SourceType, (basestring, NoneType))
        test_util.assertIsInstance(product.PriceCode, basestring)
        test_util.assertIsInstance(product.Price, float)

    def test_basic(self):
        """
        Test the default, 'send everything' behavior.
        """
        for product in self.api.get_source_code_products():
            self._check_api_output(product)


class TestDoOrderTransaction(unittest.TestCase):
    """
    Tests LigonierAPI.do_order_transaction().
    """

    def setUp(self):
        self.api = interface_factory.get_interface_obj()
        self.bluefin_api = interface_factory.get_bluefin_interface_obj()

        self.item_list = [
            {
                'product_code': 'HOL01CC',
                'quantity': 1,
                'comment': '',
                'source_code': 'WEBORDER',
                'price_code': 'STANDARD',
                'price': Decimal('19.20'),
            },
            {
                'product_code': 'ABO01BH',
                'quantity': 1,
                'comment': '',
                'source_code': 'WEBORDER',
                'price_code': 'STANDARD',
                'price': Decimal('12.00'),
            },
        ]
        self.item_total = sum([
            self.item_list[0]['price'], self.item_list[1]['price']
        ])

    def _check_api_output(self, transaction):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(transaction.AccountNumber, long)
        test_util.assertIsInstance(transaction.DocumentNumber, long)
        test_util.assertIsInstance(transaction.BatchNumber, long)
        test_util.assertIsInstance(transaction.AddressId, long)

    def test_basic(self):
        """
        Test the default, 'send everything' behavior.
        """
        account = _create_random_test_account(self.api)

        is_valid = self.api.appeal_check(
            account.AccountNumber,
            account.ShipStudentAddress.AddressId,
            'WEBORDER',
        )
        self.assertTrue(is_valid)

        tax_costs = self.api.get_tax(
            amount=self.item_total,
            zip_code=22042,
        )

        shipping_cost = self.api.get_shipping_cost(
            weight=30,
            zip_code='22042-3144',
            shipping_method='UPS-G',
        )

        total_amount = self.item_total + tax_costs.TotalTax + shipping_cost

        bluefin_trans_id, bluefin_auth_code = do_bluefin_transaction()

        result = self.api.do_order_transaction(
            account_number=account.AccountNumber,
            shipping_address_id=account.ShipStudentAddress.AddressId,
            # See self.item_list in self.setUp for an example. Or docs.
            items=self.item_list,
            bluefin_trans_id=bluefin_trans_id,
            bluefin_auth_code=bluefin_auth_code,
            shipping_amount=shipping_cost,
            shipping_method='UPS-G',
            sales_tax_1=tax_costs.SalesTax1,
            sales_tax_2=tax_costs.SalesTax2,
            sales_tax_3=tax_costs.SalesTax3,
            sales_tax_4=tax_costs.SalesTax4,
            sales_tax_5=tax_costs.SalesTax5,
            total_amount=total_amount,
        )
        self._check_api_output(result)

    def test_duplicate_guestbook(self):
        """
        Free resource orders can only be done once per person. This appears
        to be address-based. Even though we're creating a new account, this
        should fail, since other test accounts have already taken advantage
        of the offer.
        """

        account = _create_random_test_account(self.api)

        bluefin_trans_id, bluefin_auth_code = do_bluefin_transaction()

        item_list = [{
            'product_code': 'GUESTBOOK',
            'quantity': 1,
            'comment': '',
            'source_code': 'GUESTBOOK',
            'price_code': 'P',
            'price': Decimal('0.0'),
        }]

        # Another account has already taken advantage of this. This should fail.
        self.assertRaises(GuestbookDuplicateError,
            self.api.do_order_transaction,
                account_number=account.AccountNumber,
                shipping_address_id=account.ShipStudentAddress.AddressId,
                items=item_list,
                bluefin_trans_id=bluefin_trans_id,
                bluefin_auth_code=bluefin_auth_code,
                shipping_amount=Decimal('0.0'),
                shipping_method='FREELIB',
                sales_tax_1=Decimal('0.0'),
                sales_tax_2=Decimal('0.0'),
                sales_tax_3=Decimal('0.0'),
                sales_tax_4=Decimal('0.0'),
                sales_tax_5=Decimal('0.0'),
                total_amount=Decimal('0.0'),
                source_code='GUESTBOOK'
        )

    def test_duplicate_appeal(self):
        """
        appeal_check() makes sure a user has only taken advantage of an
        appeal once. Make sure this protection is working.
        """

        account = _create_random_test_account(self.api)

        # TODO: This always fails right now, since it compares addresses, and
        # all of the test accounts have the same address. We'll want to figure
        # out some way to actually test this.
        #self.api.appeal_check(
        #    account_number=account.AccountNumber,
        #    shipping_address_id=account.ShipStudentAddress.AddressId,
        #    source_code='WEBDAA',
        #)

        bluefin_trans_id, bluefin_auth_code = do_bluefin_transaction()

        item_list = [{
            'product_code': 'APPEAL',
            'quantity': 1,
            'comment': '',
            'source_code': 'WEBDAA',
            'price_code': 'P',
            'price': Decimal('10.0'),
        }]

        result = self.api.do_order_transaction(
            account_number=account.AccountNumber,
            shipping_address_id=account.ShipStudentAddress.AddressId,
            items=item_list,
            bluefin_trans_id=bluefin_trans_id,
            bluefin_auth_code=bluefin_auth_code,
            shipping_amount=Decimal('0.0'),
            shipping_method='FREELIB',
            sales_tax_1=Decimal('0.0'),
            sales_tax_2=Decimal('0.0'),
            sales_tax_3=Decimal('0.0'),
            sales_tax_4=Decimal('0.0'),
            sales_tax_5=Decimal('0.0'),
            total_amount=Decimal('10.0'),
        )
        self._check_api_output(result)

        self.assertRaises(InvalidAppealException,
            self.api.appeal_check,
                account_number=account.AccountNumber,
                shipping_address_id=account.ShipStudentAddress.AddressId,
                source_code='WEBDAA',
        )

    def test_gift_card(self):
        """
        Test the default, 'send everything' behavior.
        """
        account = _create_random_test_account(self.api)

        # Library rate is $3.99. We should probably use getShippingOptions
        # for this in the tests eventually.
        shipping_total = Decimal('3.99')
        # Hardcoded price for ABO. get_products with a filter specified would
        # be safer.
        item_total = Decimal('19.20')

        # Get a Tax data structure.
        tax = self.api.get_tax(shipping_total + item_total, account.ShipStudentAddress.Zip)
        # Tax data structure breaks taxes down into components, so add them
        # together to get total tax.
        tax_total = tax.SalesTax1 + tax.SalesTax2 + tax.SalesTax3 + tax.SalesTax4 + tax.SalesTax5
        # This is the final total for the order.
        total = shipping_total + item_total + tax_total

        bluefin_trans_id, bluefin_auth_code = do_bluefin_transaction()

        # This is set up to replicate one of the errors found on Ligonier.org.
        item_list = [
            {
                'product_code': 'ABO01CC',
                'quantity': 1,
                'comment': '',
                'source_code': 'WEBORDER',
                'price_code': 'STANDARD',
                'price': item_total,
            },
        ]

        result = self.api.do_order_transaction(
            account_number=account.AccountNumber,
            shipping_address_id=account.ShipStudentAddress.AddressId,
            items=item_list,
            bluefin_trans_id=bluefin_trans_id,
            bluefin_auth_code=bluefin_auth_code,
            shipping_amount=shipping_total,
            shipping_method='LIBRARY',
            sales_tax_1=tax.SalesTax1,
            sales_tax_2=tax.SalesTax2,
            sales_tax_3=tax.SalesTax3,
            sales_tax_4=tax.SalesTax4,
            sales_tax_5=tax.SalesTax5,
            # Entire order is paid for by the gift cert.
            gift_card_amount=total,
            # Since gift card pays for all, total becomes 0.
            total_amount=Decimal('0.0'),
        )
        self._check_api_output(result)

    def test_omit_account_num(self):
        """
        Intentionally omit account number. Should raise an exception.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.do_order_transaction,
                account_number='',
                shipping_address_id=1234567,
                items=self.item_list,
                bluefin_trans_id='',
                bluefin_auth_code='',
                shipping_amount=Decimal('6.04'),
                shipping_method='UPS-G',
                sales_tax_1=Decimal('1.87'),
                sales_tax_2=Decimal('0.31'),
                sales_tax_3=Decimal('0.0'),
                sales_tax_4=Decimal('0.0'),
                sales_tax_5=Decimal('0.0'),
                total_amount=Decimal('39.41'),
        )

    def test_negative_amount(self):
        """
        Test sending a negative amount.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.do_order_transaction,
                account_number=1234567,
                shipping_address_id=12345678,
                items=self.item_list,
                bluefin_trans_id='',
                bluefin_auth_code='',
                shipping_amount=Decimal('6.04'),
                shipping_method='UPS-G',
                sales_tax_1=Decimal('1.87'),
                sales_tax_2=Decimal('0.31'),
                sales_tax_3=Decimal('0.0'),
                sales_tax_4=Decimal('0.0'),
                sales_tax_5=Decimal('0.0'),
                total_amount=Decimal('-39.41'),
        )

    def test_omitted_amount(self):
        """
        Test omitting an amount.
        """
        self.assertRaises(APIInvalidInputException,
            self.api.do_order_transaction,
                account_number=12345678,
                shipping_address_id=12345678,
                items=self.item_list,
                bluefin_trans_id='',
                bluefin_auth_code='',
                shipping_amount=Decimal('6.04'),
                shipping_method='UPS-G',
                sales_tax_1=Decimal('1.87'),
                sales_tax_2=Decimal('0.31'),
                sales_tax_3=Decimal('0.0'),
                sales_tax_4=Decimal('0.0'),
                sales_tax_5=Decimal('0.0'),
                total_amount=None,
        )
