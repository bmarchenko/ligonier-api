from ligonier_api.calls.base import BaseAPICall

class GetSourceCodeProducts(BaseAPICall):
    """
    API call for getting source codes for all products.

    Last known signature in the WSDL::

        getSourceCodeProducts()
    """

    @property
    def service_method(self):
        return self.api.get_service.getSourceCodeProducts

    def _post_process(self, response):
        """
        Strip the outer layer off so we're just returning a list of
        ``SourceProducts`` objects, instead of a list of a list of
        ``SourceProducts`` objects.
        """
        return response.SourceProducts