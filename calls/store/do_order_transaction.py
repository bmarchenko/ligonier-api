from ligonier_api.calls.base import BaseAPICall
from ligonier_api.calls.store.exceptions import GuestbookDuplicateError
from ligonier_api.exceptions import APIErrorFromServiceException, APIInvalidInputException

class DoOrderTransaction(BaseAPICall):
    """
    API call for conducting an order transaction.

    Last known signature in the WSDL::

        OrderTransaction(
            xs:long accountNumber,
            xs:long addressId,
            xs:string items,
            xs:string token,
            xs:decimal shippingAmount,
            xs:string shippingMethod,
            xs:string mediaCode,
            xs:string sourceCode,
            xs:string projectCode,
            xs:decimal salesTax1,
            xs:decimal salesTax2,
            xs:decimal salesTax3,
            xs:decimal salesTax4,
            xs:decimal salesTax5,
            xs:decimal totalAmount,
            xs:long Batch,
            xs:string warehouse,
        )
    """
    @property
    def service_method(self):
        return self.api.set_service.OrderTransaction

    def _parse_items_list(self):
        """
        Parses through the 'items' param, which should be a list of product
        dicts. Picks this apart to form the pipe-delimited list that is sent
        to the middleware as 'items'.

        :raises: APIInvalidInputException if anything fishy is found.
        """
        item_list = self.kwargs.get('items')

        if not isinstance(item_list, list):
            raise APIInvalidInputException(
                "Invalid 'items' param (not a list): %s" % item_list
            )

        soap_item_list = self.api.set_service_client.factory.create(
            'ArrayOfOrder'
        )

        for item in item_list:
            soap_item = self.api.set_service_client.factory.create('Order')

            soap_item.ProductNumber = item['product_code']
            soap_item.Quantity = '%d' % item['quantity']
            soap_item.Comment = item.get('comment', '')
            soap_item.SourceCode = item['source_code']
            soap_item.PriceCode = item['price_code']
            soap_item.Price = "%.2f" % item['price']

            if not item['price_code']:
                # If we don't specify a price code, the middleware tosses an error.
                raise APIInvalidInputException(
                    'price_code was omitted for product %s' % item['product_code'])

            soap_item_list.Order.append(soap_item)

        # Substitute the items SOAP array as the items param.
        self.kwargs['items'] = soap_item_list

    def _pre_process(self):
        """
        Perform some validation.
        """
        self._validate_account_number()
        self._validate_account_number(
            attrib_name='gifteeAccountNumber',
            is_required=False,
        )

        self._validate_dollar_amount('shippingAmount')
        self._validate_dollar_amount('salesTax1')
        self._validate_dollar_amount('salesTax2')
        self._validate_dollar_amount('salesTax3')
        self._validate_dollar_amount('salesTax4')
        self._validate_dollar_amount('salesTax5')
        self._validate_dollar_amount('totalAmount')

        self._parse_items_list()

        if not self.kwargs.get('mediaCode'):
            self.kwargs['mediaCode'] = ''

        if not self.kwargs.get('sourceCode'):
            self.kwargs['sourceCode'] = ''

        if not self.kwargs.get('projectCode'):
            self.kwargs['projectCode'] = ''

        if not self.kwargs.get('gifteeAccountNumber'):
            self.kwargs['gifteeAccountNumber'] = 0

        if not self.kwargs.get('giftCardAmount'):
            self.kwargs['giftCardAmount'] = 0.0

        if not self.kwargs.get('batch'):
            self.kwargs['batch'] = 0

        if not self.kwargs.get('warehouse'):
            self.kwargs['warehouse'] = ''

    def _post_process(self, response):
        """
        Checks for the presence of the ``ErrorMessage`` attrib on the response.

        :raises: APIErrorFromServiceException if an error was returned.
        """

        error_msg = getattr(response, 'ErrorMessage', None)
        error_code = getattr(response, 'ErrorCode', None)

        if error_code == '-1005':
            # Someone has already done a guestbook entry for their street address.
            raise GuestbookDuplicateError(error_msg)
        elif error_msg:
            # ErrorMessage key was present on the StudentDetail object returned
            # from the service. This is only present when something bad
            # happens, so raise an exception.
            raise APIErrorFromServiceException(
                '%s (Error Code: %s)' % (error_msg, error_code),
            )

        return response