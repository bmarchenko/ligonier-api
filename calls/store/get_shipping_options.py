from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import APIInvalidInputException, APIErrorFromServiceException

class GetShippingOptions(BaseAPICall):
    """
    API call for getting the available shipping options for a weight/zip code.

    Last known signature in the WSDL::

        getShippingOptions(
            xs:string key,
            xs:string zipCode,
            xs:decimal weight,
        )
    """

    @property
    def service_method(self):
        return self.api.get_service.getShippingOptions

    def _pre_process(self):
        """
        Perform some validation.
        """
        weight = self.kwargs.get('weight')
        zip_code = self.kwargs.get('zipCode')

        try:
            self.kwargs['weight'] = float(weight)
        except ValueError:
            raise APIInvalidInputException(
                'get_tax: Invalid weight: %s' % weight)

        if weight < 0:
            raise APIInvalidInputException(
                'get_tax: Negative weight: %s' % weight)

        # Dirty, dirty hack, but middleware doesn't like more than a few
        # decimal places. BadStatusLine bug manifests itself.
        self.kwargs['weight'] = '%.2f' % self.kwargs['weight']

        if zip_code == '' or not zip_code:
            raise APIInvalidInputException('get_tax: No value for zip_code.')

        self.kwargs['zipCode'] = self._zipcode_yank_plus4(zip_code)

    def _post_process(self, response):
        """
        Checks the response for an empty string, which usually means the
        provided zip code was bogus. If the response is valid, hand processing
        off to :py:meth:`_pretty_up_response`, which returns a generator.

        :param response: The response from the remote service.
        :raises: APIErrorFromServiceException
        :returns: A generator of shipping options.
        """
        error_code = getattr(response.ShippingOptions[0], 'ErrorCode', False)
        if error_code:
            raise APIErrorFromServiceException(
                response.ShippingOptions[0].ErrorMessage
            )
        
        return self._pretty_up_response(response)

    def _pretty_up_response(self, response):
        """
        Parse floats and fake boolean for Recommended.

        :returns: A list of shipping options.
        """
        options = []

        for option in response.ShippingOptions:
            self.cast_attr_to_dollars(option, 'ShippingPrice')
            # Fake boolean coming out of API.
            recommended = getattr(option, 'Recommended', False)
            option.Recommended = recommended == "(Recommended)"
            options.append(option)

        return options