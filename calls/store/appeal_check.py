from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import InvalidAppealException

class AppealCheck(BaseAPICall):
    """
    This call is used to check a few bits of information over before
    orders are sent in. This is done before Bluefin authorizes a
    transaction in order to catch issues before there is already a hold
    on the customer's credit card.

    Last known signature in the WSDL::

        appealCheck(
            xs:string key,
            xs:string accountNumber,
            xs:long addressId,
            xs:string sourceCode,
        )
    """

    @property
    def service_method(self):
        return self.api.set_service.appealCheck

    def _pre_process(self):
        """
        Perform some validation.
        """
        self._validate_account_number()

    def _post_process(self, response):
        """
        Checks the response to see if there is or isn't an account number
        that is equal to ``None``. No account number means no match.

        :raises: StudentNotFoundException when no account number match
            could be found.
        """

        message_code = getattr(response, 'MessageCode', None)
        message = getattr(response, 'Message', None)

        if message_code == '-1001':
            raise InvalidAppealException(message)
        elif message_code == '-1002':
            raise InvalidAppealException(message)

        return getattr(response, 'RetBool', False)
