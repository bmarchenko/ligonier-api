"""
Exceptions that are specific to the store module.
"""
from ligonier_api.exceptions import APIInvalidInputException

class GuestbookDuplicateError(APIInvalidInputException):
    """
    Raised when a user tries to take advantage of a free resource, when someone
    from their address has already done so.
    """

    pass
