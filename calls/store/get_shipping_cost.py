from decimal import Decimal
from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import APIInvalidInputException

class GetShippingCost(BaseAPICall):
    """
    API call for getting the shipping cost for an order.

    Last known signature in the WSDL::

        getShipping(
            xs:string zipCode,
            xs:decimal weight,
            xs:string shippingMethod,
        )

    Response ends up just being a float. No special SOAP type on return, so
    post-processing is just error checking.
    """

    @property
    def service_method(self):
        return self.api.get_service.getShipping

    def _pre_process(self):
        """
        Perform some validation.
        """

        weight = self.kwargs.get('weight')
        zip_code = self.kwargs.get('zipCode')

        try:
            self.kwargs['weight'] = float(weight)
        except ValueError:
            raise APIInvalidInputException(
                'get_tax: Invalid weight: %s' % weight)

        if weight < 0:
            raise APIInvalidInputException(
                'get_tax: Negative weight: %s' % weight)

        if zip_code == '' or not zip_code:
            raise APIInvalidInputException(
                'get_shipping_cost: No value for zip_code.'
            )

    def _post_process(self, response):
        """
        A return value of -1004.0 means invalid shipping method.
        """

        if response == -1004.0:
            raise APIInvalidInputException(
                'Invalid shipping method: %s' % self.kwargs.get('shippingMethod')
            )

        return Decimal(str(response)).quantize(Decimal('0.01'))