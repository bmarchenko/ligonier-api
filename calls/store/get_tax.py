from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import APIInvalidInputException, APIErrorFromServiceException

class GetTax(BaseAPICall):
    """
    API call for getting the tax for an order amount for a particular
    zip code.

    Last known signature in the WSDL::

        GetTax(
            xs:int amt,
            xs:string zipCode,
        )

    Response ends up just being a float. No special SOAP type, no post-processing
    is needed.
    """

    @property
    def service_method(self):
        return self.api.get_service.getTax

    def _pre_process(self):
        """
        Perform some validation.
        """
        amount = self.kwargs.get('amt')
        self.kwargs['amt'] = self.clean_dollar_amount(amount)

        zip_code = self.kwargs.get('zipCode')
        zip_code = self._zipcode_yank_plus4(zip_code)

        try:
            self.kwargs['zipCode'] = int(zip_code)
        except ValueError:
            raise APIInvalidInputException(
                'get_tax: Invalid zip_code: %s' % zip_code)

    def _post_process(self, response):
        """
        Checks for the presence of an error code.

        :raises: APIErrorFromServiceException if an error was found.
        """

        if response.ErrorCode is not None:
            if response.ErrorCode == '-1000':
                # Just don't have any tax records for this zip code. We'll
                # let the 0 amount fall through silently.
                pass
            else:
                msg = "%s (Error Code: %s)" % (
                    response.ErrorMessage,
                    response.ErrorCode,
                )
                raise APIErrorFromServiceException(msg)

        self.cast_attr_to_dollars(response, 'TotalTax')
        self.cast_attr_to_dollars(response, 'SalesTax1')
        self.cast_attr_to_dollars(response, 'SalesTax2')
        self.cast_attr_to_dollars(response, 'SalesTax3')
        self.cast_attr_to_dollars(response, 'SalesTax4')
        self.cast_attr_to_dollars(response, 'SalesTax5')

        return response