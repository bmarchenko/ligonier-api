from ligonier_api.calls.base import BaseAPICall

class GetProducts(BaseAPICall):
    """
    API call for getting the tax for an order amount for a particular
    zip code.

    Last known signature in the WSDL::

        getProducts(
            xs:int amt,
            ArrayOfString productNumbers,
        )
    """

    # These response bodies are huge.
    supress_response_body_logging = True

    @property
    def service_method(self):
        return self.api.get_service.getProducts

    def _pre_process(self):
        """
        The 'productNumbers' kwarg may optionally contain a list of product
        codes to filter the results by. We convert this to a comma-delimited
        string and send it to the SOAP service, instead of using something
        list-like.
        """

        # The list of product codes passed to the get_products() call. If this
        # has any members, the request will only return the requested products.
        only_these = self.kwargs.get('productNumbers', '')
        # Determine if the user passed a list of product codes, or nothing.
        if only_these and isinstance(only_these, list):
            # User passed a list of product codes, create a string and covnert
            # the list to a comma-delimited string.

            filter_str = ''

            # Append each member of the list to the specialized SOAP structure.
            counter = 0
            for product_code in only_these:
                if counter > 0:
                    filter_str += ','
                filter_str += product_code
                counter += 1
        else:
            # No products specified to filter by, API needs an empty string
            # or it'll error out.
            filter_str = ''

        self.kwargs['productNumbers'] = filter_str

    def _post_process(self, response):
        """
        This call has some 1 == True, 0 == False fields that don't return
        normal Python booleans.
        """

        product_filters = self.kwargs.get('productNumbers', '')
        # Did the user ask for a subset of products?
        was_filtered = bool(product_filters)
        # Middleware likes to spit out dupes sometimes. We'll track here to
        # make sure that doesn't happen.
        dupe_tracker = dict()

        for product in response.Products:
            if dupe_tracker.has_key(product.ItemNumber):
                # Dupe encountered, don't yield it.
                continue
            if not product.ItemNumber:
                # Apparently, some of these can come back with a None value.
                continue
            if was_filtered and product.ItemNumber not in product_filters:
                # We filtered the product list, but received something we
                # didn't ask for. Don't yield this random product.
                continue

            # Negative inventory levels are possible from GP.
            product.QuantityOnHand = max(0, product.QuantityOnHand)
            # We've seen this lig code, mark it in the dupe tracker so this
            # will be the only copy of this yielded.
            dupe_tracker[product.ItemNumber] = True

            yield product