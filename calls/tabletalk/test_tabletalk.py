import unittest

from ligonier_api.tests import test_util, interface_factory, test_defines
from ligonier_api.tests.test_util import _create_random_test_account

class TestDoSubscriptionTransaction(unittest.TestCase):
    """
    Tests LigonierAPI.do_subscription_transaction().
    """
    def setUp(self):
        self.api = interface_factory.get_interface_obj()
        self.bluefin_api = interface_factory.get_bluefin_interface_obj()

    def _check_api_output(self, transaction):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(transaction.AccountNumber, long)
        test_util.assertIsInstance(transaction.DocumentNumber, long)
        test_util.assertIsInstance(transaction.BatchNumber, long)
        test_util.assertIsInstance(transaction.AddressId, long)
        test_util.assertIsInstance(transaction.TransactionReturn, basestring)

    def test_basic(self):
        """
        Test the default, 'send everything' behavior.
        """

        account = _create_random_test_account(self.api)

        entitlements = self.api.get_student_entitlements(
            account_number=account.AccountNumber,
        )
        self.assertEqual(entitlements, [])

        bluefin_response = self.bluefin_api.send_request({
            'pay_type': 'C',
            'tran_type': 'S',
            'amount': 138.00,
            'card_number': test_defines.TEST_CC_NUM,
            'card_expire': test_defines.TEST_CC_EXPIR,
            'disable_cvv2': 'true',
            'bill_name1': 'First',
            'bill_name2': 'Last',
            'bill_street': '123 Some Street',
            'bill_zip': '12345',
            # Two-letter country code.
            # http://www.iso.org/iso/en/prods-services/iso3166ma/index.html
            'bill_country': 'US',
        })
        bluefin_trans_id = bluefin_response['trans_id']
        bluefin_auth_code = bluefin_response['auth_code']

        subscribers = [
            {
                'account_number': account.AccountNumber,
                'title': account.Title,
                'first_name': account.FirstName,
                'last_name': account.LastName,
                'address1': account.ShipStudentAddress.AddressLine1,
                'address2': '',
                'city': account.ShipStudentAddress.City,
                'country': account.ShipStudentAddress.Country,
                'state': account.ShipStudentAddress.State,
                'zip_code': account.ShipStudentAddress.Zip,
                'num_issues': '12',
                'price_code': '1YR-USA',
                'amount': 23.00,
                'copy_count': 1,
            },
            {
                'title': 'Mr.',
                'first_name': 'Kelly',
                'last_name': 'Chestnut',
                'address1': '6032 Linneal Beach Dr.',
                'address2': '',
                'city': 'Apopka',
                # Test to make sure this is getting converted to USA.
                'country': 'US',
                'state': 'FL',
                'zip_code': '32708',
                'num_issues': '12',
                'price_code': '1YR-USA',
                'amount': 23.00,
                'copy_count': 1,
            },
        ]

        result = self.api.do_subscription_transaction(
            account_number=account.AccountNumber,
            bluefin_trans_id=bluefin_trans_id,
            bluefin_auth_code=bluefin_auth_code,
            subscribers=subscribers,
        )
        self._check_api_output(result)

        entitlements = self.api.get_student_entitlements(
            account_number=account.AccountNumber,
        )
        self.assertGreater(len(entitlements), 0)
