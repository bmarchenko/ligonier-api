from ligonier_api.calls.base import BaseAPICall
from ligonier_api.exceptions import APIErrorFromServiceException, APIInvalidInputException


class DoSubscriptionTransaction(BaseAPICall):
    """
    API call for conducting a Tabletalk subscription transaction.

    Last known signature in the WSDL::

        SubscrTransaction(
            xs:long accountNumber,
            xs:string sourceCode,
            xs:string mediaCode,
            xs:string subscriber,
            xs:string token,
            xs:string authCode,
        )
    """
    @property
    def service_method(self):
        return self.api.set_service.SubscrTransaction

    def _parse_subscriber_list(self):
        """
        Parses through the 'subscriber' param, which should be a list of subscriber
        dicts. Picks this apart to form the pipe-delimited list that is sent
        to the middleware as 'subscriber'.

        :raises: APIInvalidInputException if anything fishy is found.
        """
        subscriber_list = self.kwargs.get('subscriber')

        if not isinstance(subscriber_list, list):
            raise APIInvalidInputException(
                "Invalid 'subscribers' param (not a list): %s" % subscriber_list
            )

        # This is a SOAP buffer object (which acts like a list) that we'll
        # toss subscribers in.
        soap_subscriber_list = self.api.set_service_client.factory.create(
            'ArrayOfSubscriber'
        )

        for item in subscriber_list:
            soap_item = self.api.set_service_client.factory.create(
                'Subscriber'
            )

            soap_item.Title=item['title']
            soap_item.FirstName=item['first_name']
            soap_item.LastName=item['last_name']
            soap_item.AddressLine1=item['address1']
            soap_item.AddressLine2=item.get('address2', '')
            soap_item.Country=item['country']
            soap_item.City=item['city']
            soap_item.State=item['state']
            soap_item.ZipCode=item['zip_code']
            soap_item.PriceCode = item['price_code']
            soap_item.NumberOfIssues=int(item['num_issues'])
            soap_item.CopyCount = int(item['copy_count'])
            soap_item.Price=float(item['amount'])
            soap_item.AccountNumber=item.get('account_number', 0)

            if soap_item.Country == 'US':
                soap_item.Country = 'USA'

            soap_subscriber_list.Subscriber.append(soap_item)

        # Set the subscriber SOAP array as the subscriber param.
        self.kwargs['subscriber'] = soap_subscriber_list

    def _pre_process(self):
        """
        Perform some validation.
        """
        self._validate_account_number()

        if not self.kwargs.get('sourceCode'):
            self.kwargs['sourceCode'] = ''

        if not self.kwargs.get('mediaCode'):
            self.kwargs['mediaCode'] = ''

        self._parse_subscriber_list()

    def _post_process(self, response):
        """
        Checks for the presence of the ``ErrorMessage`` attrib on the response.

        :raises: APIErrorFromServiceException if an error was returned.
        """
        error_msg = getattr(response, 'ErrorMessage', None)
        if error_msg:
            # ErrorMessage key was present on the StudentDetail object returned
            # from the service. This is only present when something bad
            # happens, so raise an exception.
            raise APIErrorFromServiceException(
                '%s (Error Code: %s)' % (error_msg, response.ErrorCode),
            )

        return response