from ligonier_api.calls.address.exceptions import InvalidAddressError
from ligonier_api.calls.base import BaseAPICall


class VerifyAddressCall(BaseAPICall):
    """
    API call for CASS-certifying addresses.

    Last known signature in the WSDL::

        VerifyAddress(
            xs:string addressLine1,
            xs:string addressLine2,
            xs:string city,
            xs:string state,
            xs:string zip,
        )
    """

    @property
    def service_method(self):
        return self.api.get_service.VerifyAddress

    def _pre_process(self):
        address_1 = self.kwargs.get('addressLine1')
        if isinstance(address_1, basestring):
            # Trailing spaces make the middleware bug out.
            self.kwargs['addressLine1'] = address_1.strip()
        if not address_1:
            raise InvalidAddressError('No address_line_1 value provided.')

        address_2 = self.kwargs.get('addressLine2')
        if isinstance(address_2, basestring):
            # Trailing spaces make the middleware bug out.
            self.kwargs['addressLine2'] = address_2.strip()
        elif address_2 is None:
            # API server doesn't understand None, make sure this sends as
            # an empty string.
            self.kwargs['addressLine2'] = ''

        state = self.kwargs.get('state')
        # Middleware flips its lid if we pass it a None/Null, so pass an
        # empty string instead.
        self.kwargs['state'] = state or ''

    def _post_process(self, response):
        """
        Stuffs everything into a dict.
        """

        # TODO: Raise exceptions if error_code is encountered.

        return {
            'address1': response.AddressLine1,
            'address2': response.AddressLine2,
            'city': response.City,
            'state': response.State,
            'country': response.Country,
            'postal_code': response.Zip,
            'plus4': response.Plus4Code,
            'error_code': response.ErrorCode,
            'error_message': response.ErrorMessage,
        }