from ligonier_api.calls.base import BaseAPICall

class GetCountries(BaseAPICall):
    """
    API call for getting a list of countries from DS.

    Last known signature in the WSDL::

        getCountry(
            xs:int amt,
        )
    """

    # These response bodies are huge.
    supress_response_body_logging = True

    @property
    def service_method(self):
        return self.api.get_service.getCountry

    def _post_process(self, response):
        """
        This call has some 1 == True, 0 == False fields that don't return
        normal Python booleans.
        """

        for country in response.Country:

            yield {
                'abbreviation': country.Value,
                'name': country.Description,
                'last_modified_dtime': country.ModDate,
            }