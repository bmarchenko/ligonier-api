"""
Exceptions that are specific to address validation.
"""
from ligonier_api.exceptions import APIInvalidInputException

class InvalidAddressError(APIInvalidInputException):
    """
    Raised when an invalid address is passed in.
    """
    pass


class InvalidShippingAddressError(InvalidAddressError):
    """
    Raised when an invalid address is passed in.
    """
    pass


class InvalidBillingAddressError(InvalidAddressError):
    """
    Raised when an invalid address is passed in.
    """
    pass