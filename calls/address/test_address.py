import unittest
from ligonier_api.calls.address.exceptions import InvalidAddressError
from types import NoneType

from ligonier_api.tests import test_util, interface_factory


class TestGetCountries(unittest.TestCase):
    """
    Tests LigonierAPI.get_countries().
    """
    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, country):
        """
        Checks output for validity.
        """
        test_util.assertIsInstance(country['abbreviation'], basestring)
        test_util.assertIsInstance(country['name'], (basestring, NoneType))
        test_util.assertIsInstance(country['last_modified_dtime'], (basestring, NoneType))

    def test_correct(self):
        """
        Nothing special here, just make sure it's working with normal
        valid values. The address coming back should be the same.
        """
        for country in self.api.get_countries():
            self._check_api_output(country)


class TestVerifyAddress(unittest.TestCase):
    """
    Tests LigonierAPI.verify_address().
    """

    def setUp(self):
        self.api = interface_factory.get_interface_obj()

    def _check_api_output(self, address):
        """
        Checks output for validity.
        """

        test_util.assertIsInstance(address['address1'], basestring)
        test_util.assertIsInstance(address['address2'], (basestring, NoneType))
        test_util.assertIsInstance(address['city'], basestring)
        test_util.assertIsInstance(address['state'], basestring)
        test_util.assertIsInstance(address['postal_code'], basestring)
        test_util.assertIsInstance(address['plus4'], (basestring, NoneType))
        test_util.assertIsInstance(address['country'], (basestring, NoneType))
        test_util.assertIsInstance(address['error_code'], (basestring, NoneType))
        test_util.assertIsInstance(address['error_message'], (basestring, NoneType))

    def test_correct(self):
        """
        Nothing special here, just make sure it's working with normal
        valid values. The address coming back should be the same.
        """

        address1 = self.api.verify_address(
            address_1='1273 Dillon Road',
            city='Austell',
            state='GA',
            zip_code='30168',
        )
        self._check_api_output(address1)

    def test_missing_address1(self):
        """
        Test behavior for completely omitting address 1.
        """

        self.assertRaises(InvalidAddressError,
            self.api.verify_address,
                address_1='',
                city='Austell',
                state='GA',
                zip_code='30168',
        )

    def test_noexist_address1(self):
        """
        Provide some bogus addresses. This sometimes causes a really long
        response time.
        """

        self.api.verify_address(
            address_1='129 Doesntexist St.',
            city='Greenville',
            state='SC',
            zip_code='29615-123',
        )

    def test_omit_state(self):
        """
        Test behavior for completely omitting the state. This covers a bug
        where the middleware would hang and break the connection instead of
        returning an error message.
        """

        address1 = self.api.verify_address(
            address_1='1273 Dillon Road',
            city='Austell',
            state=None,
            zip_code='30168',
        )
        # "We have not been able to validate your state."
        self.assertEqual(address1['error_code'], "-1005")

    def test_omit_number(self):
        """
        Intentionally omit the address number in address1.
        """

        # This doesn't return an error code. Perhaps it should. We'll test
        # for any changes in behavior, at least.
        address1 = self.api.verify_address(
            address_1='Heritage Hills Dr',
            city='Seneca',
            state='SC',
            zip_code='29678',
        )
        self._check_api_output(address1)

if __name__ == '__main__':
    unittest.main()
