import base64
from xml.sax import SAXParseException
from suds.plugin import MessagePlugin
from suds.sax.parser import Parser


class HandlePDF(MessagePlugin):
    def received(self, context):
        parser = Parser()

        try:
            parser.parse(string=context.reply)
        except SAXParseException:
            base64_reply = base64.b64encode(context.reply)
            reply  = "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
            reply += "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">"
            reply += "<soap:Body>"
            reply += "<getStudentYearEndStatementResponse xmlns=\"http://tempuri.org/\">"
            reply += "<getStudentYearEndStatementResult>"
            reply += base64_reply
            reply += "</getStudentYearEndStatementResult>"
            reply += "</getStudentYearEndStatementResponse>"
            reply += "</soap:Body>"
            reply += "</soap:Envelope>"
            context.reply = reply
