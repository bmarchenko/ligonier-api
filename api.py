"""
End users should go through this module to make API calls. Instantiate a
:py:class:`ligonier_api.api.LigonierAPI` object and make the calls through
its methods. Here's a really basic example::

    from ligonier_api.api import LigonierAPI

    api = LigonierAPI()
    response = api.verify_address(
        'firstname',
        'lastname',
        'address1',
        'address2',
        'city',
        'state',
        'postalcode',
    )
    print response
"""
from ligonier_api.calls.member.set_student_address import CreateStudentAddress
from ligonier_api.calls.member.set_student_email import SetStudentEmail

import os
from ligonier_api.plugins.handle_pdf import HandlePDF
from suds import client
from ligonier_api.calls.address.get_countries import GetCountries
from ligonier_api.calls.address.verify_address import VerifyAddressCall
from ligonier_api.calls.donation.do_ministry_partner_transaction import DoMinistryPartnerTransaction
from ligonier_api.calls.member.get_student_address_list import GetStudentAddressList
from ligonier_api.calls.member.get_student_email_list import GetStudentEmailList
from ligonier_api.calls.member.get_student_phone_list import GetStudentPhoneList
from ligonier_api.calls.member.set_student_phone import SetStudentPhone
from ligonier_api.calls.member.set_active_phone import SetActivePhone
from ligonier_api.calls.member.set_active_email import SetActiveEmail
from ligonier_api.calls.member.set_primary_phone import SetPrimaryPhone
from ligonier_api.calls.member.get_student_year_end_statement import GetStudentYearEndStatement
from ligonier_api.calls.store.appeal_check import AppealCheck
from ligonier_api.calls.store.get_tax import GetTax
from ligonier_api.calls.gift_card.get_student_gift_card_summary import GetStudentGiftCardSummary
from ligonier_api.calls.store.get_shipping_cost import GetShippingCost
from ligonier_api.calls.store.get_shipping_options import GetShippingOptions
from ligonier_api.calls.store.get_products import GetProducts
from ligonier_api.calls.store.get_source_code_products import GetSourceCodeProducts
from ligonier_api.calls.member.get_or_pop_student_details import GetOrPopulateStudentDetails
from ligonier_api.calls.member.is_student import IsStudent
from ligonier_api.calls.member.get_student import GetStudent
from ligonier_api.calls.member.get_student_address import GetStudentAddress
from ligonier_api.calls.member.get_student_donation_summary import GetStudentDonationSummary
from ligonier_api.calls.member.get_student_order_summary import GetStudentOrderSummary
from ligonier_api.calls.store.do_order_transaction import DoOrderTransaction
from ligonier_api.calls.donation.do_donate_transaction import DoDonateTransaction
from ligonier_api.calls.event.do_event_transaction import DoEventTransaction
from ligonier_api.calls.tabletalk.do_subscription_transaction import DoSubscriptionTransaction
from ligonier_api.calls.gift_card.do_create_gift_card_transaction import DoCreateGiftCardTransaction
from ligonier_api.calls.gift_card.do_gift_card_transaction import DoGiftCardTransaction
from ligonier_api.calls.gift_card.get_gift_card_amount import GetGiftCardAmount
from ligonier_api.calls.magplus.get_student_entitlements import GetStudentEntitlements
from ligonier_api.calls.magplus.do_mag_publish import DoMagPublish


class LigonierAPI(object):
    """
    Ligonier API object for querying the API server. Instantiate it and use
    the public, documented members to issue queries. To see the output,
    save it to a variable and print the result of the query. Output may change
    at any time based on what's going on upstream at the API server.


    * **ConnectorGet**: Contains methods that just retrieve info
    This class currently uses two different SOAP services in its methods:rmation from
      Donor Studio and GP, without modifying it.
    * **ConnectorInquiry**: Contains methods that set or modify data in
      Donor Studio and GP.

    Each call points at one of these services, behind the scenes.
    """

    def __init__(self, api_key, enable_wsdl_caching=False,
                 get_service_wsdl_uri=None, set_service_wsdl_uri=None,
                 inquiry_service_wsdl_uri=None,
                 post_response_callback=None):
        """
        :param str api_key: The authentication key to send to the server. This
            acts like a password, and determines whether the client has
            access.
        :param bool enable_wsdl_caching: When ``True``, cache the WSDLs from
            the remote service.
        :keyword str get_service_wsdl_uri: The URI to the ConnectorGet API's
            WSDL. This service handles most of the methods that return, but
            don't modify, data. This contains the host/port and API
            call details.
        :keyword str set_service_wsdl_uri: The URI to the ConnectorSet API's
            WSDL. This service handles most of the methods that set or modify
            data. This contains the host/port and API call details.
        :keyword str inquiry_service_wsdl_uri: The URI to the ConnectorInquiry
            API's WSDL. This service handles most of the methods that set and
            retrieve data. This contains the host/port and API call details.
        :keyword callable post_response_callback: A callable that is called
            after the response comes back from the middleware for all methods
            in this connection object.
        """

        if not get_service_wsdl_uri:
            get_service_wsdl_uri = os.environ.get(
                'LIGONIER_API_GET_WSDL',
                'http://localhost:9569/ConnectorGet.asmx?WSDL'
            )

        if not set_service_wsdl_uri:
            set_service_wsdl_uri = os.environ.get(
                'LIGONIER_API_SET_WSDL',
                'http://localhost:9569/ConnectorSet.asmx?WSDL'
            )

        if not inquiry_service_wsdl_uri:
            inquiry_service_wsdl_uri = os.environ.get(
                'LIGONIER_API_INQ_WSDL',
                'http://localhost:9569/ConnectorInquiry.asmx?WSDL'
            )

        # This is the same, regardless of which service.
        self.api_key = api_key

        handle_pdf_plugin = HandlePDF()

        self.get_service_client = client.Client(get_service_wsdl_uri, plugins=[handle_pdf_plugin])
        self.set_service_client = client.Client(set_service_wsdl_uri, plugins=[handle_pdf_plugin])
        self.inquiry_service_client = client.Client(inquiry_service_wsdl_uri, plugins=[handle_pdf_plugin])

        if not enable_wsdl_caching:
            self.get_service_client.set_options(cache=None)
            self.set_service_client.set_options(cache=None)
            self.inquiry_service_client.set_options(cache=None)

        self.get_service = self.get_service_client.service
        self.set_service = self.set_service_client.service
        self.inquiry_service = self.inquiry_service_client.service

        self.post_response_callback = post_response_callback

    def appeal_check(self, account_number, shipping_address_id, source_code):
        """
        This call is used to check a few bits of information over before
        orders are sent in. This is done before Bluefin authorizes a
        transaction in order to catch issues before there is already a hold
        on the customer's credit card.

        :param account_number: The student's account number.
        :type account_number: int or basestring
        :param int shipping_address_id: Use :py:meth:`get_student_address` to
            figure out an address ID.
        :param str source_code: The source code being passed with the order.
        """

        return AppealCheck(self,
            accountNumber=account_number,
            addressId=shipping_address_id,
            sourceCode=source_code,
        ).get_response()

    def verify_address(self, address_1, city, state, zip_code,
                       address_2=None):
        """
        This call CASS-certifies addresses. Returned value is the suggested
        address to use.

        :param str address_1: The first address line.
        :keyword str address_2: The address's second line. (optional)
        :param str city: The address's city.
        :param str state: The address's state.
        :param str zip_code: The address's zip code.
        :rtype: dict
        :returns: The suggested address to use. Last known format is as
            follows (may be out of date).

        ::
            {
                "address1": "1273 Dillon Rd",
                "address2": None,
                "city": "Austell",
                "state": "GA",
                "postal_code": "30168",
                "plus4": "1234",
                "error_code": None,
                "error_message": None,
            }
        """

        return VerifyAddressCall(self,
            addressLine1=address_1,
            addressLine2=address_2,
            city=city,
            state=state,
            zip=zip_code
        ).get_response()

    def get_tax(self, amount, zip_code):
        """
        Given an order total, and a zip code, calculate the tax.

        :param Decimal amount: The total amount.
        :param str zip_code: The zip code of the delivery address.
        :returns: A single ``Tax`` SOAP object as shown below.

        ::

            (Tax){
                TotalTax = 1.2
                SalesTax1 = 0.8
                SalesTax2 = 0.4
                SalesTax3 = 0.0
                SalesTax4 = 0.0
                SalesTax5 = 0.0
                ErrorCode = None
                ErrorMessage = None
            }

        :raises: :py:exc:`ligonier_api.exceptions.APIInvalidInputException`
            when an invalid amount or zip_code are specified.
            :py:exc:`ligonier_api.exceptions.APIErrorFromServiceException`
            when an error comes back from the API server.
        """

        return GetTax(self,
            amt=amount,
            zipCode=zip_code
        ).get_response()

    def get_shipping_cost(self, weight, zip_code, shipping_method):
        """
        Given the total weight of an order, calculate the cost for shipping.

        :param float weight: The total weight of the order.
        :param str zip_code: The zip code of the delivery address.
        :param str shipping_method: One of the 'ShippingCode' values from
            :py:meth:`get_shipping_options` with the same weight/zip code
            values.
        :rtype: float
        :returns: The total shipping amount.
        :raises: :py:exc:`ligonier_api.exceptions.APIInvalidInputException`
            when an invalid weight, zip_code, or shipping_method are specified.
        """

        return GetShippingCost(self,
            weight=weight,
            zipCode=zip_code,
            shippingMethod=shipping_method,
        ).get_response()

    def get_shipping_options(self, weight, zip_code):
        """
        Given a shipment weight and a zip code, return the possible shipping
        options for delivery.

        :param float weight: The total weight of the order.
        :param str zip_code: The zip code of the delivery address.
        :returns: A list of shipping options for the given weight and
            zip code, with each member being in the following format

        ::

            (ShippingOptions){
                ShippingPrice = "78.15"
                ShippingDescription = "UPS-2DAAM"
                Recommended = True
                ShippingCode = "UPS-2DAAM"
                ErrorCode = 0
            },

        :raises: :py:exc:`ligonier_api.exceptions.APIInvalidInputException`
            when an invalid weight or zip_code are specified.
        """

        return GetShippingOptions(self,
            weight=weight,
            zipCode=zip_code
        ).get_response()

    def get_products(self, only_these=None):
        """
        Returns a generator of Ligonier products, along with some important
        details from GP and Donor Direct. In the case where you pass an
        invalid product in ``only_these``, said invalid product will be
        ignored, and all valid matches will be returned.

        .. warning:: This response weighs in well over a meg, and is somewhat
            taxing on the API server. Be careful with the frequency in which
            this is called.

        :keyword list only_these: A list of product code strings. This will
            be used as a filter, and only the requested products will
            be returned.
        :returns: A generator of Ligonier products, with each member being
            in the following format:

        ::

            (Products){
                ItemNumber = "ABO01CC"
                ItemDescription = "Abortion CD Collection"
                QuantityOnHand = 83
                ItemWeight = 0
                StandardPrice = 19.2
                BookStorePrice = 12.0
                ChurchPrice = 13.2
                PastorPrice = 13.2
                PromoPrice = 16.8
                ListPrice = 19.2
                UPC = "881658002611"
                ISBN = None
                AllowBackOrder = True
                WebSite = True
                Taxable = True
                SourceCode = None
                ErrorCode = "0:
                ErrorMessage = None
            },
        """

        return GetProducts(self, productNumbers=only_these).get_response()

    def get_countries(self):
        """
        Returns a generator of country data dicts. This comes from DS, and is
        used to drive the country drop-down for our various forms.

        :rtype: generator
        :returns: A generator of country data dicts, as seen below:

        ::

            [
                {
                    'abbreviation': 'USA',
                    'name': 'United States of America',
                    'last_modified_dtime': datetime.datetime(...),
                },

                ...
            ]
        """

        return GetCountries(self).get_response()

    def get_source_code_products(self):
        """
        Returns a list of source codes, which products they apply to, and
        the nature of any associated discounts.

        :returns: A list of ``SourceProducts`` SOAP objects in the form of:

        ::

            (SourceProducts){
                Description = "$10 Off Coupon"
                ProductCode = "CPN$OFF"
                SourceCode = "SAVE10"
                SourceType = "$CPN"
                PriceCode = "STANDARD"
                Price = -10.0
            },
        """

        return GetSourceCodeProducts(self).get_response()

    def is_student(self, first_name=None, last_name=None,
                   address_1=None, email=None):
        """
        Given a student's information, this call searches Donor Studio for
        a student match. If a match is found, a handful of details regarding
        their account are returned.

        It is important to note that you need not pass all of the keyword
        args to this method. The more data you can provide, the greater chance
        of a match.

        .. warning:: This call appears to take a best-case guess when there
            isn't enough data to tie-break between multiple matches. For
            example, lassing 'Test', 'Guy' for first and last names would
            pick and return only one of the people named 'Test Guy', assuming
            there is more than one person with the same name in DS.

        :keyword str first_name: Student's first name.
        :keyword str last_name: Student's last name.
        :keyword str address_1: Student's first address line.
        :keyword str email: Student's email address.
        :returns: If no match was found, ``False`` is returned. Otherwise,
            a ``StudentAccount`` SOAP object is returned, detailed below:

        ::

            (StudentAccount){
                AccountNumber = 23093044
                RecordId = 904958
                DocumentNumber = 0
                AddressId = 23095118
                BatchNumber = 0
                SourceCode = None
                FirstName = "Test"
                LastName = "Guy"
                Email = "snagglepants2@gmail.com"
                PhoneNumber = None
                AccountType = "I"
                PartnerStatus = True
                Active = True
                ErrorMessage = "  AccountNumber: 23093062
                Name: Test Guy
                Jan 26 2012  3:24PM -AS01: The address is valid and deliverable.**Addr:SHIP
                ***
                "
            }
        """

        return IsStudent(self,
            firstName=first_name,
            lastName=last_name,
            address1=address_1,
            email=email,
        ).get_response()

    def get_student(self, account_number, first_name=None, last_name=None):
        """
        If we know a student exists, and have their account number, this is
        the fastest way to get details for their account. Also, unlike
        :py:meth:`get_or_populate_student_details`, this call is read-only.

        The only required argument is ``account_number``, first and last
        name can be omitted.

        :param account_number: The student's account number.
        :type account_number: int or basestring
        :keyword str first_name: Student's first name.
        :keyword str last_name: Student's last name.
        :returns: If no match was found, ``False`` is returned. Otherwise,
            a ``StudentAccount`` SOAP object is returned, detailed below:

        ::

            (StudentAccount){
                AccountNumber = 23051918
                RecordId = 4592739
                DocumentNumber = None
                AddressId = None
                BatchNumber = None
                SourceCode = None
                FirstName = "Test"
                LastName = "Guy2"
                Email = "test@guy.com"
                PhoneNumber = None
                Title = "Mr."
                AccountType = "I"
                Active = True
                ErrorMessage = None
            }

        :raises: :py:exc:`ligonier_api.exceptions.StudentNotFoundException`
            when no student with the given account number could be found.
        """
        return GetStudent(self,
            firstName=first_name,
            lastName=last_name,
            accountNumber=account_number,
        ).get_response()

    def get_student_email_list(self, account_number):
        """
        Retrieves the email addresses associated with a student.

        :param account_number: The student's account number.
        :type account_number: int or basestring
        :rtype: list
        :returns: A list of email address dicts as shown below.

        ::

            [
                {
                    'active_email': True,
                    'email_type': PRIVATE,
                    'use_as_primary': True,
                    'account_number': 23119941L,
                    'record_id': 392967L,
                    'email_address': 23e697c2567f512438993cf46c9ee53c@somewhere.com
                },
            ]


        :raises: :py:exc:`ligonier_api.exceptions.StudentNotFoundException`
            when no student with the given account number could be found.
        """

        return GetStudentEmailList(self,
            accountNumber=account_number,
        ).get_response()

    def get_student_phone_list(self, account_number):
        """
        Retrieves the phone numbers associated with a student.

        :param account_number: The student's account number.
        :type account_number: int or basestring
        :rtype: list
        :returns: A list of phone number dicts as shown below.

        ::

            [
                {
                    'phone_number': 8888888,
                    'phone_type': HOME,
                    'international_code': None,
                    'active_phone': True,
                    'account_number': 0L,
                    'extension': None,
                    'record_id': 667325L,
                    'use_as_primary': True,
                    'area_code': 864
                }
            ]

        :raises: :py:exc:`ligonier_api.exceptions.StudentNotFoundException`
            when no student with the given account number could be found.
        """

        return GetStudentPhoneList(self,
            accountNumber=account_number,
        ).get_response()

    def set_student_email(self, account_number, emailaddress, primaryemail):
        """
        Sets phone number associated with a student.

        :param int account_number: The student's account number.
        :param basestring internationalcode: International calling code for the phone number.
        :param basestring areacode: Area code for canadian and US numbers. There might be others that can use it as well.
        :param basestring phonenumber: The 7-digit number and allowed 10 before DS will truncate the number.
        :param basestring extension: The separate number for dialing into a corporate phone system.

        ::

        (StudentPhone){
           AccountNumber = 0
           RecordId = 0
           InternationCode = None
           AreaCode = "89"
           PhoneNumber = "988"
           Extension = "8989"
           UseAsPrimary = 0
           Active = 1
        }

        :raises: :py:exc:`ligonier_api.exceptions.APIErrorFromServiceException`
        if an error is returned from the Ligonier SOAP service.
        """

        return SetStudentEmail(self, accountNumber=account_number, emailAddress=emailaddress, primaryEmail=primaryemail).get_response()

    def set_active_email(self, emailrecordid, setactiveemail):
        """
        Sets phone number associated with a student.

        :param int phonerecordid: The recordId that identifies the phone number
            that needs to be either Activated with a 1 or InActive with a 0.
        :param int setactivephone: 0 or 1, Active = 1 and InActive = 0.

        ::

        (StudentPhone){
           AccountNumber = 0
           RecordId = 0
           InternationCode = None
           AreaCode = "89"
           PhoneNumber = "988"
           Extension = "8989"
           UseAsPrimary = 0
           Active = 1
        }

        :raises: :py:exc:`ligonier_api.exceptions.APIErrorFromServiceException`
        if an error is returned from the Ligonier SOAP service.
        """
        return SetActiveEmail(self,
                              emailRecordId=emailrecordid,
                              setActiveEmail=setactiveemail
        ).get_response()

    def set_student_phone(self, phonenumber, account_number, internationalcode, areacode, extension):
        """
        Sets phone number associated with a student.

        :param int account_number: The student's account number.
        :param basestring internationalcode: International calling code for the phone number.
        :param basestring areacode: Area code for canadian and US numbers. There might be others that can use it as well.
        :param basestring phonenumber: The 7-digit number and allowed 10 before DS will truncate the number.
        :param basestring extension: The separate number for dialing into a corporate phone system.

        ::

        (StudentPhone){
           AccountNumber = 0
           RecordId = 0
           InternationCode = None
           AreaCode = "89"
           PhoneNumber = "988"
           Extension = "8989"
           UseAsPrimary = 0
           Active = 1
        }

        :raises: :py:exc:`ligonier_api.exceptions.APIErrorFromServiceException`
        if an error is returned from the Ligonier SOAP service.
        """
        return SetStudentPhone(self,
            accountNumber=account_number,
            internationalCode=internationalcode,
            areaCode=areacode,
            phoneNumber=phonenumber,
            extension=extension
        ).get_response()

    def set_active_phone(self, phonerecordid, setactivephone):
        """
        Sets phone number associated with a student.

        :param int phonerecordid: The recordId that identifies the phone number
            that needs to be either Activated with a 1 or InActive with a 0.
        :param int setactivephone: 0 or 1, Active = 1 and InActive = 0.

        ::

        (StudentPhone){
           AccountNumber = 0
           RecordId = 0
           InternationCode = None
           AreaCode = "89"
           PhoneNumber = "988"
           Extension = "8989"
           UseAsPrimary = 0
           Active = 1
        }

        :raises: :py:exc:`ligonier_api.exceptions.APIErrorFromServiceException`
        if an error is returned from the Ligonier SOAP service.
        """
        return SetActivePhone(self,
                              phoneRecordId=phonerecordid,
                              setActivePhone=setactivephone
        ).get_response()

    def set_primary_phone(self, phonerecordid):
        """
        Sets phone number associated with a student.

        :param int phonerecordid: The recordId that identifies the phone number
            that needs to be either Activated with a 1 or InActive with a 0.
        :param int setactivephone: 0 or 1, Active = 1 and InActive = 0.

        ::

        (StudentPhone){
           AccountNumber = 0
           RecordId = 0
           InternationCode = None
           AreaCode = "89"
           PhoneNumber = "988"
           Extension = "8989"
           UseAsPrimary = 0
           Active = 1
        }

        :raises: :py:exc:`ligonier_api.exceptions.APIErrorFromServiceException`
        if an error is returned from the Ligonier SOAP service.
        """

        return SetPrimaryPhone(self,
                              phoneRecordId=phonerecordid,
        ).get_response()

    def get_student_address_list(self, account_number):
        """
        Retrieves a list of StudentAddress objects for an account.

        :param account_number: The student's account number.
        :type account_number: int or basestring
        :rtype: list
        :returns: A list of address dicts, as seen below.

        ::

            [
                {
                    'address_id': 23159594L,
                    'PhoneNumber': None,
                    'plus4_code': None,
                    'city': Austell,
                    'zip': 31068,
                    'country': 'US',
                    'state': GA,
                    'record_id': 930038L,
                    'address_type': BILL,
                    'address_line_2': 1273 Dillon Rd,
                    'address_line_1': 1273 Dillon Rd
                },
                {
                    'address_id': 23159595L,
                    'PhoneNumber': None,
                    'plus4_code': None,
                    'city': Austell,
                    'zip': 31068,
                    'country': 'US',
                    'state': GA,
                    'record_id': 930038L,
                    'address_type': SHIP,
                    'address_line_2': 1274 Dillon Rd,
                    'address_line_1': 1274 Dillon Rd
                }
            ]

        :raises: :py:exc:`ligonier_api.exceptions.StudentNotFoundException`
            when no student with the given account number could be found.
        """

        return GetStudentAddressList(self,
            accountNumber=account_number,
        ).get_response()

    def get_student_address(self, account_number, address_1=None):
        """
        Returns the billing or shipping address associated with a student's
        account.

        :param account_number: The student's account number.
        :type account_number: int or basestring
        :keyword str address_1: A first address line to match on.
        :returns: If an account number match is found, a ``StudentAddress``
            object, shown below, is returned.

        ::

            (StudentAddress){
                AddressLine1 = "1273 Dillon Rd"
                AddressLine2 = None
                City = "Austell"
                State = "GA"
                Zip = "30168-5203"
                AddressType = "BILL"
                AddressErrorString = None
                Plus4Code = None
                Country = None
                AddressId = 23054020
                RecordId = 4592738
                ErrorCode = "0"
                ErrorMessage = None
            }

        :raises: :py:exc:`ligonier_api.exceptions.APIInvalidInputException`
            if an invalid account number, primary arg, or address type
            is specified. :py:exc:`ligonier_api.exceptions.StudentNotFoundException`
            when no student with the given account number could be found.
        """

        return GetStudentAddress(self,
            accountNumber=account_number,
            addressLine1=address_1,
        ).get_response()

    def set_student_address(self, account_number, address_type, first_name, last_name, country, address_line1, address_line2, city, state, zip_code):
        """
        Sets phone number associated with a student.

        :param int account_number: The student's account number.
        :param basestring internationalcode: International calling code for the phone number.
        :param basestring areacode: Area code for canadian and US numbers. There might be others that can use it as well.
        :param basestring phonenumber: The 7-digit number and allowed 10 before DS will truncate the number.
        :param basestring extension: The separate number for dialing into a corporate phone system.

        ::

        (StudentPhone){
           AccountNumber = 0
           RecordId = 0
           InternationCode = None
           AreaCode = "89"
           PhoneNumber = "988"
           Extension = "8989"
           UseAsPrimary = 0
           Active = 1
        }

        :raises: :py:exc:`ligonier_api.exceptions.APIErrorFromServiceException`
        if an error is returned from the Ligonier SOAP service.
        """

        return CreateStudentAddress(self, accountNumber=account_number,
                               addressType=address_type,
                               firstName=first_name,
                               lastName=last_name,
                               country=country,
                               addressLine1=address_line1,
                               addressLine2=address_line2,
                               city=city,
                               state=state,
                               zipCode=zip_code).get_response()

    def get_student_entitlements(self, account_number):
        """
        Given an account number, this call returns their Mag+ entitlements,
        which are issue IDs available on Mag+.

        :param account_number: The student's account number.
        :rtype: list
        :returns: If no match was found, an empty list is returned. Otherwise,
            return a list of issue IDs as strings.
        """

        return GetStudentEntitlements(self,
            accountNumber=account_number,
        ).get_response()

    def get_student_donation_summary(self, account_number):
        """
        Retrieves a student's order history.

        :param account_number: The student's account number.
        :type account_number: int or basestring
        :returns: If an account number match is found, a list of
            ``DonationSummary`` SOAP objects (as shown below) are returned.

        ::

            (DonationSummary){
                Description = "DEFAULT CODE _ ENTER CORRECT PC"
                Title = None
                GiftDate = 2011-09-12
                GiftAmount = 100.0
             },

        :raises: :py:exc:`ligonier_api.exceptions.APIInvalidInputException`
            if an invalid account number is specified.
            :py:exc:`ligonier_api.exceptions.StudentNotFoundException` if the
            middleware is unable to find a student with the specified
            account number. A
            :py:exc:`ligonier_api.exceptions.APIErrorFromServiceException`
            is raised when the API returns an unrecognized error.
        """

        return GetStudentDonationSummary(self,
            accountNumber=account_number,
        ).get_response()

    def get_student_order_summary(self, account_number, days_back=180):
        """
        Retrieves a student's order history.

        :param account_number: The student's account number.
        :type account_number: int or basestring
        :keyword int days_back: The number of days to go back in the order
            history.
        :returns: If an account number match is found, a list of
            ``OrderSummary`` SOAP objects (as shown below) are returned.

        ::

            (OrderSummary){
                ProductCode = "HOL12BPP"
                Description = "Holiness of God Pocket-Size"
                OrderDate = 8210-01-20
                Price = 0.0
                Quantity = 5948
                ExtPrice = 0.0
                ItemStatus = "Shipped"
                ShipDate = None
                ShippingMethod = None
                TrackingNumber = None
                GPsop = "OR00873824"
                OrderId = 2039419
                DocumentNumber = 2039419
                Amount = 0.0
                ShippingAmount = 0.0
                TaxAmount = 0.0
             },

        :raises: :py:exc:`ligonier_api.exceptions.APIInvalidInputException`
            if an invalid account number is specified.
            :py:exc:`ligonier_api.exceptions.StudentNotFoundException` if the
            middleware is unable to find a student with the specified
            account number. A
            :py:exc:`ligonier_api.exceptions.APIErrorFromServiceException`
            is raised when the API returns an unrecognized error.
        """

        return GetStudentOrderSummary(self,
            accountNumber=account_number,
            daysBack=days_back,
        ).get_response()

    def get_student_year_end_statement(self, account_number, year):
        """
        Retrieves a year end statement for the student in PDF format.

        :param account_number: The student's account number.
        :type account_number: int or basestring
        :keyword int year: The year to retrieve the statement for.
        :rtype: file-like
        :returns: A file-like object containing a PDF.
        """

        return GetStudentYearEndStatement(self,
            accountNumber=account_number,
            year=year,
        ).get_response()

    def get_or_populate_student_details(self, page_type, title,
        first_name, last_name, email,
        ship_country, ship_address_1, ship_city, ship_state, ship_zip,
        bill_country, bill_address_1, bill_city, bill_state, bill_zip,
        area_code=None, phone_number=None,
        ship_address_2=None, bill_address_2=None, account_number=None,
        source_code=None):
        """
        Given a student's information, this call searches Donor Studio for
        a student match and does one of two things:

            * If a match is found, returns the student's details. If there
              was a match, but the addresses differ, the new address is added
              as another address.
            * If no match was found, creates a new student account and
              returns the details.

        :param basestring page_type: Used to determine which source code to use.
            Must be one of: ``DONATE``, ``EVENT``, ``ORDER``, ``PLEDGE``,
            ``RYM``, ``SUBSCR``.
        :param basestring title: The student's salutation/title. See
            :ref:`titles` for valid values.
        :param basestring first_name: Student's first name.
        :param basestring last_name: Student's last name.
        :param basestring email: The student's email address.
        :param basestring ship_country: The shipping address country value from
            Ligonier's ``A03_AddressMaster`` table.
        :param basestring ship_address_1: Shipping address, line 1.
        :keyword str ship_address_2: Shipping address, line 2. (optional)
        :param basestring ship_city: Shipping city.
        :param basestring ship_zip: Shipping zip code.
        :param basestring bill_country: The billing address country value from
            Ligonier's ``A03_AddressMaster`` table.
        :param basestring bill_address_1: Billing address, line 1.
        :keyword str bill_address_2: Billing address, line 2. (optional)
        :param basestring bill_city: Billing city.
        :param basestring bill_zip: Billing zip code.
        :keyword basestring area_code: The user's area code, if we have it.
        :keyword basestring phone_number: The user's phone number, if we have it.
        :keyword basestring account_number: In the case that we know the student's
            account number, pass it to help match the details to an account
            in Donor Studio. For new students, omit this.
        :keyword basestring source_code: An optional source code override. Typically,
            ``page_type`` is used to generate the source code, and this
            arg is not needed.
        :returns: An object with the details of the student that was looked
            up, updated, or created:

        ::

            (StudentDetail){
                PartnerStatus = True
                AccountNumber = 23051918
                RecordId = None
                Title = "Mr."
                FirstName = "Test"
                LastName = "Guy"
                Email = "test@guy.com"
                ProjectCode = "DEFAULT"
                SourceCode = "WEBDONATE"
                BillStudentAddress =
                    (StudentAddress){
                        AddressLine1 = "1273 Dillon Rd"
                        AddressLine2 = None
                        City = "Austell"
                        State = "GA"
                        Zip = "30168-5203"
                        AddressType = "BILL"
                        AddressErrorString = None
                        Plus4Code = None
                        Country = "US"
                        AddressId = 23054020
                        RecordId = 4592738
                        ErrorCode = "0"
                        ErrorMessage = None
                    }
                ShipStudentAddress =
                    (StudentAddress){
                        AddressLine1 = "1273 Dillon Rd"
                        AddressLine2 = None
                        City = "Austell"
                        State = "GA"
                        Zip = "30168-5203"
                        AddressType = "BILL"
                        AddressErrorString = None
                        Plus4Code = None
                        Country = "US"
                        AddressId = 23054020
                        RecordId = 4592738
                        ErrorCode = "0"
                        ErrorMessage = None
                    }
            }

        :raises: :py:exc:`ligonier_api.exceptions.APIErrorFromServiceException`
            if an error is returned from the Ligonier SOAP service.
        """

        return GetOrPopulateStudentDetails(self,
            pageType=page_type,
            title=title,
            firstName=first_name,
            lastName=last_name,
            email=email,
            areaCode=area_code,
            phoneNumber=phone_number,
            accountNumber=account_number,
            sourceCode=source_code,
            shipCountry=ship_country,
            shipAddressLine1=ship_address_1,
            shipAddressLine2=ship_address_2,
            shipCity=ship_city,
            shipState=ship_state,
            shipZip=ship_zip,
            billCountry=bill_country,
            billAddressLine1=bill_address_1,
            billAddressLine2=bill_address_2,
            billCity=bill_city,
            billState=bill_state,
            billZip=bill_zip,
        ).get_response()

    def do_create_pledge_transaction(self, account_number,
        student_title, student_first_name, student_last_name,
        amount, source_code='WEBPLEDGE', comment=None, project_code=None):
        """
        Creates a pledge.

        :param account_number: The student's account number. Use
            :py:meth:`is_student` or :py:meth:`get_or_populate_student_details`
            to determine this.
        :type account_number: int or basestring
        :param str student_title: A valid title for the student.
        :param str student_first_name: First name of the student.
        :param str student_last_name: Last name of the student.
        :param float amount: The total amount of the pledge.
        :keyword str source_code: Optionally, override the default
            'WEBPLEDGE' source code.
        :keyword str comment: Not sure what this does.
        :returns: A ``TransactionSummary`` SOAP objects, as shown below.

        ::

            (TransactionSummary){
                AccountNumber = 23093085
                DocumentNumber = 4300228
                BatchNumber = 65049
                AddressId = 0
                TransactionReturn = "23093085"
            }


        :raises: :py:exc:`ligonier_api.exceptions.APIInvalidInputException`
            if an invalid value is specified.
        """

        return DoMinistryPartnerTransaction(self,
            accountNumber=account_number,
            sourceCode=source_code,
            mediaId='UN',
            projectCode=project_code,
            title=student_title,
            firstName=student_first_name,
            lastName=student_last_name,
            useOnDay=1,
            comment=comment,
            creditCard='',
            expMonth='',
            expYear='',
            cCID='',
            nameOnCard='',
            amount=amount,
        ).get_response()

    def do_create_recurring_donation_transaction(self, account_number,
        student_title, student_first_name, student_last_name,
        cc_number, cc_exp_month, cc_exp_year, cc_cvn, cc_full_name,
        amount, source_code='WEBPLEDGE', project_code=None,
        comment=None, day_to_bill=1):
        """
        Creates a recurring donation.

        :param account_number: The student's account number. Use
            :py:meth:`is_student` or :py:meth:`get_or_populate_student_details`
            to determine this.
        :type account_number: int or basestring
        :param str student_title: A valid title for the student.
        :param str student_first_name: First name of the student.
        :param str student_last_name: Last name of the student.
        :param str cc_number: The credit card number to bill.
        :param str cc_exp_month: The expiration month (two digit).
        :param str cc_exp_year: The expiration year (two digit).
        :param str cc_cvn: The card's CVN security number.
        :param str cc_full_name: The full name on the card.
        :param float amount: The total amount of the pledge.
        :keyword str source_code: Optionally, override the default
            'WEBPLEDGE' source code.
        :keyword str project_code: Optional override. Omitting lets the
            middleware determine this.
        :keyword int day_to_bill: Day of month to do the EFT. This needs to
            be either 1 or 15, nothing else.
        :keyword str comment: Not sure what this does.
        :returns: A ``TransactionSummary`` SOAP objects, as shown below.

        ::

            (TransactionSummary){
                AccountNumber = 23093088
                DocumentNumber = 4300231
                BatchNumber = 65049
                AddressId = 0
                TransactionReturn = "23093088"
             }

        :raises: :py:exc:`ligonier_api.exceptions.APIInvalidInputException`
            if an invalid value is specified.
        """

        return DoMinistryPartnerTransaction(self,
            accountNumber=account_number,
            sourceCode=source_code,
            mediaId='UN',
            projectCode=project_code,
            title=student_title,
            firstName=student_first_name,
            lastName=student_last_name,
            useOnDay=day_to_bill,
            comment=comment,
            creditCard=cc_number,
            expMonth=cc_exp_month,
            expYear=cc_exp_year,
            cCID=cc_cvn,
            nameOnCard=cc_full_name,
            amount=amount,
        ).get_response()

    def do_donate_transaction(self, account_number, bluefin_trans_id,
                              bluefin_auth_code, total_amount, project_code,
                              source_code, media_code=None, comment=None):
        """
        Processes a donate transaction. Requires bits of data from
        other API calls, as well as the Bluefin API.

        :param account_number: The student's account number. Use
            :py:meth:`is_student` or :py:meth:`get_or_populate_student_details`
            to determine this.
        :type account_number: int or basestring
        :param str bluefin_trans_id: The Bluefin response's trans_id value.
        :param str bluefin_auth_code: The Bluefin response's auth_code value.
        :param float total_amount: The total amount of the donation.
        :param str project_code: Project code for the donation.
        :param str source_code: Source code for the donation.
        :keyword str media_code: Optional media code for the donation.
        :keyword str comment: Customer's comments to go with donation.
        :returns: A ``TransactionSummary`` SOAP objects, as shown below.

        ::

            (TransactionSummary){
                AccountNumber = 23051918
                DocumentNumber = 3624183
                BatchNumber = 49666
                AddressId = 0
            }

        :raises: :py:exc:`ligonier_api.exceptions.APIInvalidInputException`
            if an invalid value is specified.
        """

        return DoDonateTransaction(self,
            accountNumber=account_number,
            comment=comment,
            mediaCode=media_code,
            sourceCode=source_code,
            projectCode=project_code,
            token=bluefin_trans_id,
            authCode=bluefin_auth_code,
            totalAmount=total_amount,
        ).get_response()

    def do_event_transaction(self, account_number, bluefin_trans_id,
                             bluefin_auth_code, total_amount, price_code,
                             event_code, attendees, media_code=None):
        """
        Processes a event registration transaction. Requires bits of data from
        other API calls, as well as the Bluefin API.

        :param account_number: The student's account number. Use
            :py:meth:`is_student` or :py:meth:`get_or_populate_student_details`
            to determine this.
        :type account_number: int or basestring
        :param str bluefin_trans_id: The Bluefin response's trans_id value.
        :param str bluefin_auth_code: The Bluefin response's auth_code value.
        :param float total_amount: The total amount of all registrations.
        :param str price_code: Price code for the registration. See GDocs.
        :param str event_code: Unique identifier for the event.
        :param list attendees: A list of attendees in the format
            of::

                [
                    {
                        'title': 'Mr.',
                        'first_name': 'Kelly',
                        'last_name': 'Carper',
                        'email': 'kcarper@yahoo.com',
                        'session_code': 'MAIN',
                        # Optional, can be omitted.
                        'source_code': 'WEBEVENT',
                        # See Middleware docs for more on this.
                        'price_code': 'REG-STU',
                        'price': 69.00,
                        # Provide it if we have it. Can be omitted.
                        'account_number': '',
                    },
                    ...
                ]

        :returns: A ``TransactionSummary`` SOAP objects, as shown below.

        ::

            (TransactionSummary){
                AccountNumber = 23051918
                DocumentNumber = 3625349
                BatchNumber = 49814
                AddressId = 0
                EventCode = "FAL11"
                TransactionReturn = "23052126, 23052127"
            }

        :raises: :py:exc:`ligonier_api.exceptions.APIInvalidInputException`
            if an invalid value for a parameter is specified.
        """

        return DoEventTransaction(self,
            accountNumber=account_number,
            eventCode=event_code,
            priceCode=price_code,
            token=bluefin_trans_id,
            authCode=bluefin_auth_code,
            authorizedAmount=total_amount,
            mediaCode=media_code,
            quantity=0,
            attendees=attendees,
        ).get_response()

    def do_order_transaction(self, account_number, shipping_address_id, items,
        bluefin_trans_id, bluefin_auth_code, shipping_amount, shipping_method,
        sales_tax_1, sales_tax_2, sales_tax_3, sales_tax_4, sales_tax_5,
        total_amount, media_code=None, source_code=None, project_code=None,
        giftee_account_number=None, gift_card_amount=0.0, batch_num=0,
        warehouse=None):
        """
        Processes a store order transaction. Requires bits of data from
        other API calls, as well as the Bluefin API.

        :param account_number: The student's account number. Use
            :py:meth:`is_student` or :py:meth:`get_or_populate_student_details`
            to determine this.
        :type account_number: int or basestring
        :param int shipping_address_id: Use :py:meth:`get_student_address` to
            figure out an address ID.
        :param list items: A list of dicts of products with purchase-associated
            information included in the format of::

                [
                    {
                        'product_code': 'HOL01CC',
                        'quantity': 1,
                        'comment': '',
                        'source_code': 'WEBORDER',
                        'price_code': 'STANDARD',
                        'price': 19.20,
                    },
                    ...
                ]

        :param str bluefin_trans_id: The Bluefin response's trans_id value.
        :param str bluefin_auth_code: The Bluefin response's auth_code value.
        :param float shipping_amount: The total shipping amount for the chosen
            ``shipping_method``. Use :py:meth:`get_shipping_options` and
            :py:meth:`get_shipping_cost` for this.
        :param str shipping_method: The chosen shipping_method.
            Use the ``ShippingCode`` attrib from the response of
            :py:meth:`get_shipping_options` for this.
        :param float sales_tax_1: Value of ``SalesTax1`` in the
            :py:meth:`get_tax` response for the address's (``address_id``)
            zip code.
        :param float sales_tax_2: Value of ``SalesTax2`` in the
            :py:meth:`get_tax` response for the address's (``address_id``)
            zip code.
        :param float sales_tax_3: Value of ``SalesTax3`` in the
            :py:meth:`get_tax` response for the address's (``address_id``)
            zip code.
        :param float sales_tax_4: Value of ``SalesTax4`` in the
            :py:meth:`get_tax` response for the address's (``address_id``)
            zip code.
        :param float sales_tax_5: Value of ``SalesTax5`` in the
            :py:meth:`get_tax` response for the address's (``address_id``)
            zip code.
        :param float total_amount: The total amount to bill for. This will be
            item total + tax + shipping - discounts.
        :keyword str media_code: Optional media code for the whole order.
        :keyword str source_code: Optional source code for the whole order.
        :keyword str project_code: Optional project code for the whole order.
        :keyword int giftee_account_number: If this order is a gift for someone,
            this should be the recipient's account number.
        :keyword float gift_card_amount: The amount of the total that will be
            debited from the gift card.
        :keyword int batch_num: An optional batch number, provided by accounting.
        :keyword str warehouse: The warehouse the item should be shipped from.
            IE: IMPACT. Leaving it blank lets middleware determine
            what should be used.
        :returns: A ``TransactionSummary`` SOAP objects, as shown below.

        ::

            (TransactionSummary){
                AccountNumber = 23051918
                DocumentNumber = 3624183
                BatchNumber = 49666
                AddressId = 23054021
            }

        :raises: :py:exc:`ligonier_api.exceptions.APIInvalidInputException`
            if an invalid account number is specified.
        """

        return DoOrderTransaction(self,
            accountNumber=account_number,
            addressId=shipping_address_id,
            items=items,
            token=bluefin_trans_id,
            authCode=bluefin_auth_code,
            shippingAmount=shipping_amount,
            shippingMethod=shipping_method,
            mediaCode=media_code,
            sourceCode=source_code,
            projectCode=project_code,
            salesTax1=sales_tax_1,
            salesTax2=sales_tax_2,
            salesTax3=sales_tax_3,
            salesTax4=sales_tax_4,
            salesTax5=sales_tax_5,
            totalAmount=total_amount,
            gifteeAccountNumber=giftee_account_number,
            giftCardAmount=gift_card_amount,
            batch=batch_num,
            warehouse=warehouse,
        ).get_response()

    def do_subscription_transaction(self, account_number, bluefin_trans_id,
                             bluefin_auth_code, subscribers, source_code='',
                             media_code=None):
        """
        Processes a Tabletalk subscription transaction. Requires bits of data from
        other API calls, as well as the Bluefin API.

        :param account_number: The student's account number. Use
            :py:meth:`is_student` or :py:meth:`get_or_populate_student_details`
            to determine this. This is the person paying for the subscriptions,
            but this person doesn't have to be one of the recipients listed in
            the ``subscribers`` parameter.
        :type account_number: int or basestring
        :param str bluefin_trans_id: The Bluefin response's trans_id value.
        :param str bluefin_auth_code: The Bluefin response's auth_code value.
        :param list subscribers: A list of subscribers in the format
            of::

                [
                    {
                        # Only pass a value here if this is the transaction
                        # payee (the one being Bluefinned). Otherwise, it's
                        # safe to omit this.
                        'account_number': '',
                        'title': 'Mr.',
                        'first_name': 'Kelly',
                        'last_name': 'Chestnut',
                        'address1': '6032 Linneal Beach Dr',
                        'address2': '',
                        'city': 'Apopka',
                        'state': 'FL',
                        'zip_code': '32708',
                        'amount': 23.00,
                        'num_issues': 12,
                        'copy_count': 1,
                        'price_code': '1YR-USA',
                    },
                    ...
                ]

        :keyword str source_code: Optional source code.
        :keyword str media_code: Optional media code.
        :returns: A ``TransactionSummary`` SOAP objects, as shown below.

        ::

            (TransactionSummary){
                AccountNumber = 23051918
                DocumentNumber = 3625370
                BatchNumber = 49818
                AddressId = 0
                TransactionReturn = "23052136, 23052137"
            }


        :raises: :py:exc:`ligonier_api.exceptions.APIInvalidInputException`
            if an invalid value for a parameter is specified.
        """

        return DoSubscriptionTransaction(self,
            accountNumber=account_number,
            sourceCode=source_code,
            mediaCode=media_code,
            subscriber=subscribers,
            token=bluefin_trans_id,
            authCode=bluefin_auth_code,
        ).get_response()

    def do_mag_publish(self, issue_id, issue_date):
        """
        Publishes a Mag+ issue.

        :param str issue_id: The ID of the issue to publish.
        :param datetime.date issue_date: The publication date of the issue.
        :raises: :py:exc:`ligonier_api.exceptions.APIInvalidInputException`
            if an invalid value is specified.
        """

        return DoMagPublish(self,
            issueId=issue_id,
            issueDate=issue_date,
        ).get_response()

    def do_create_gift_card_transaction(self, account_number, value,
                                        email=None):
        """
        Creates a new gift card for the specified account, with the given
        value.

        :param account_number: The student's account number. Use
            :py:meth:`is_student` or :py:meth:`get_or_populate_student_details`
            to determine this.
        :type account_number: int or basestring
        :param int value: The dollar amount to make the gift card worth.
        :keyword str email: The email of the user purchasing the gift card.
        :returns: A ``GiftCard`` SOAP objects, as shown below.

        ::

            (GiftCard){
                GiftCardId = 0
                GiftCardNumber = 1008
                GiftCardKey = 5400267
                Value = 10
                Amount = 0.0
                AccountNumber = 23051918
                DocumentNumber = 0
                Email = None
                ErrorCode = None
                ErrorMessage = None
            }

        :raises: :py:exc:`ligonier_api.exceptions.APIInvalidInputException`
            if an invalid value is specified.
        """

        return DoCreateGiftCardTransaction(self,
            accountNumber=account_number,
            value=value,
            email=email,
        ).get_response()

    def do_gift_card_transaction(self, account_number, order_document_number,
                                 gift_card_number, gift_card_key, amount):
        """
        Decrements a gift card's balance by the specified amount.

        :param account_number: The student's account number. Use
            :py:meth:`is_student` or :py:meth:`get_or_populate_student_details`
            to determine this.
        :type account_number: int or basestring
        :param int order_document_number: The DocumentNumber value from
            :py:meth:`do_order_transaction`.
        :param int gift_card_number: Gift card's unique identifier number.
        :param int gift_card_key: Gift card's security key.
        :param Decimal amount: Amount to reduce gift card balance by.
        :returns: A ``GiftCard`` SOAP objects, as shown below.

        ::

            (GiftCard){
                GiftCardId = 0
                GiftCardNumber = 1008
                GiftCardKey = 5400267
                Value = 10
                Amount = 0.0
                AccountNumber = 23051918
                DocumentNumber = 0
                Email = None
                ErrorCode = None
                ErrorMessage = None
            }

        :raises: :py:exc:`ligonier_api.exceptions.APIInvalidInputException`
            if an invalid value is specified.
        """

        return DoGiftCardTransaction(self,
            accountNumber=account_number,
            documentNumber=order_document_number,
            giftCardNumber=gift_card_number,
            giftCardKey=gift_card_key,
            amount=amount,
        ).get_response()

    def get_student_gift_card_summary(self, account_number):
        """
        Returns a list of gift cards that the student has associated with
        their account.

        :param account_number: The student's account number. Use
            :py:meth:`is_student` or :py:meth:`get_or_populate_student_details`
            to determine this.
        :type account_number: int or basestring
        :returns: If an account number match is found, a list of
            ``GiftCard`` SOAP objects (as shown below) are returned.

        ::

            {
                GiftCardId = 0
                GiftCardNumber = 0
                GiftCardKey = 0
                Value = 0
                Amount = 0.0
                AccountNumber = 0
                ErrorCode = "-1000"
                ErrorMessage = "DataAccess.getStudentGiftCard: No records returned please verify connection."
            }, ...

        :raises: :py:exc:`ligonier_api.exceptions.APIInvalidInputException`
            if an invalid account number is specified.
        """

        return GetStudentGiftCardSummary(self,
            accountNumber=account_number,
        ).get_response()

    def get_gift_card_amount(self, gift_card_number, gift_card_key):
        """
        Returns the gift card's remaining balance.

        :param int gift_card_number: Gift card's unique identifier number.
        :param int gift_card_key: Gift card's security key.
        :rtype: Decimal
        :returns: The card's remaining balance.
        :raises: :py:exc:`ligonier_api.exceptions.APIInvalidInputException`
            if an invalid value is specified.
            :py:exc:`ligonier_api.exceptions.GiftCardNotFoundException` if an
            invalid gift card or gift card key is given.
        """

        return GetGiftCardAmount(self,
            giftCardNumber=gift_card_number,
            giftCardKey=gift_card_key,
        ).get_response()