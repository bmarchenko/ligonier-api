"""
This module creates LigonierAPI objects for each of the unit test
modules to use. We create a new one for each unittest module to reduce any
chance of tainting tests by all of them using the same interface. IE: Values
getting modified.

See get_interface_obj() below.
"""
import sys
import os

project_root_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
if not project_root_dir in sys.path:
    sys.path.insert(0, project_root_dir)

from ligonier_api import LigonierAPI
from bluefin.directmode.clients import V3Client

# If this is a non-None value, override the API server host/port used
# in the unit tests. Tuple in the form of ('host', port)
API_SERVER_OVERRIDE = None
# The authentication key to send to the server.
API_SERVER_KEY = '5tgb5rdx5rtfv9zikm7ujm4rtfgb888'

def response_time_callback(call_name, response_time, **kwargs):
    """
    A test callback. Just be obnoxious for now, while we're preparing for
    the DS launch.
    """
    print "Response time for %s: %f" % (call_name, response_time)

def get_interface_obj():
    """
    Use this function to get a LigonierAPI object. Create new interfaces for
    each unit test module to avoid potential variable pollution.
    """
    if API_SERVER_OVERRIDE:
        # Server overridden. Use given tuple.
        wsdl_override = API_SERVER_OVERRIDE
        return LigonierAPI(
            API_SERVER_KEY,
            wsdl_uri=wsdl_override,
            post_response_callback=response_time_callback
        )

    # No overrides, use the default.
    return LigonierAPI(
        API_SERVER_KEY,
        post_response_callback=response_time_callback
    )

def get_bluefin_interface_obj():
    """
    Use this function to get a Bluefin V3Client object.Create new interfaces for
    each unit test module to avoid potential variable pollution.
    """
    return V3Client(
        account_id=120753316874,
        dynip_sec_code='FVsUVdU20Bb7ZZCl',
    )