"""
Various utility methods to assist with testing.
"""

import datetime
import hashlib
from ligonier_api.tests import test_defines, interface_factory

def assertIsInstance(obj, cls):
    """
    Tests an object (obj) to make sure that it is an instance of class (cls).
    
    Returns True if it is, and False if it isn't. Note that this takes class
    hierarchy into account. For example,
    
    >>> test_str = 'yay
    >>> isinstance(test_str, basestring)
    True
    >>> test_unicode = u'yay'
    >>> isinstance(test_unicode, basestring)
    True
    
    Notice that one is a str, the other is unicode, but they both inherit
    basestring, so they both are instances of basestring.
    """

    is_expected_type = isinstance(obj, cls)
    type_found = type(obj)
    type_expected = cls
    assert is_expected_type, "Expected %s but got %s" % (type_expected,
                                                         type_found)

def do_bluefin_transaction():
    """
    Performs a bluefin transaction and spits out a transaction ID and auth code.
    Many middleware methods require a bluefin trans ID/auth code, but don't
    really care about the specifics of the transaction. This function is
    a convenient way to spit out trans ID/auth code combos for the tests.

    :rtype: tuple
    :returns: A tuple in the form of ``(bluefin_trans_id, bluefin_auth_code)``
    """

    bluefin_api = interface_factory.get_bluefin_interface_obj()
    bluefin_response = bluefin_api.send_request({
        'pay_type': 'C',
        'tran_type': 'S',
        'amount': 138.00,
        'card_number': test_defines.TEST_CC_NUM,
        'card_expire': test_defines.TEST_CC_EXPIR,
        'disable_cvv2': 'true',
        'bill_name1': 'First',
        'bill_name2': 'Last',
        'bill_street': '123 Some Street',
        'bill_zip': '12345',
        # Two-letter country code.
        # http://www.iso.org/iso/en/prods-services/iso3166ma/index.html
        'bill_country': 'US',
    })
    return bluefin_response['trans_id'], bluefin_response['auth_code']

def _create_random_test_account(api):
    """
    Creates a random test account, for calls that can't be done multiple
    times.
    """

    first_name = hashlib.md5('%s' % datetime.datetime.now()).hexdigest()
    email = '%s@somewhere.com' % first_name

    return api.get_or_populate_student_details(
        page_type='DONATE',
        title='Mr.',
        first_name=first_name,
        last_name='Testification',
        email=email,
        area_code=864,
        phone_number=8888888,
        ship_country='US',
        ship_address_1='1274 Dillon Rd',
        ship_city='Austell',
        ship_state='GA',
        ship_zip='31068',
        bill_country='US',
        bill_address_1='1273 Dillon Rd',
        bill_city='Austell',
        bill_state='GA',
        bill_zip='31068',
        ship_address_2=None,
        bill_address_2=None,
        account_number=0,
    )