"""
Some constant definitions for unit tests to use. We have them defined here
so we may change them globally with ease.
"""

# Some test values for Bluefin credit card testing.
TEST_CC_NUM = 4242424242424242
TEST_CC_EXPIR_MONTH = '12'
TEST_CC_EXPIR_YEAR = '13'
TEST_CC_EXPIR = '%s%s' % (TEST_CC_EXPIR_MONTH, TEST_CC_EXPIR_YEAR)
TEST_CC_CVN = '123'
TEST_CC_NAME_ON_CARD = 'Test Guy'