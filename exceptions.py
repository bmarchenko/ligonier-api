"""
The following exceptions may be raised before or after an API call. All
exceptions should inherit from :py:exc:`APIException`.
"""

class APIException(Exception):
    """
    Generic API exception. All API exceptions should sub-class this.

    .. note:: Don't use this directly, sub-class it or use
        one of the other, more specific exceptions.
    """

    def __init__(self, message):
        """
        :param str message: A helpful error message to show in the logs.
        """

        # Have to do this to make the exception pickleable.
        super(APIException, self).__init__(message)

        self.message = message

    def __str__(self):
        """
        String representation shows the developer-specified error message.
        """

        # Developer-specified error message.
        return self.message


class APIInvalidInputException(APIException):
    """
    Raised when the user provides invalid input to an API call.
    """

    pass


class APIErrorFromServiceException(APIException):
    """
    Raised when some kind of error comes back from the Ligonier service that
    we don't have a specific behavior for.

    'Something bad happened, but we're not sure what it is, or how
    to handle it.'
    """

    pass


class StudentNotFoundException(APIException):
    """
    Raised when using a call that is supposed to return a student, but not
    mach was found. A good example is get_student(), where just returning
    a ``None`` value can lead to bad behaviors.
    """

    pass


class GiftCardNotFoundException(APIException):
    """
    Raised when using a call that is supposed to interact with a GiftCard,
    but no match was found.
    """

    pass


class InvalidAppealException(APIException):
    """
    Raised when an appeal_check fails.
    """

    pass